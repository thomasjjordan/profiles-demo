# The app is looking great. We have a strong need to get the app released and tested ASAP. Before we can do that though we need to ensure all text on the reservations tab is token-based.

# If possible I'd like to get this app out by the end of the week. If you think there are any items on this list that are too time-consuming let me know. Also let me know if something ends up being more complicated than it looks, I'd rather we work together to find a quicker solution than potentially miss the end-of-week deadline.

# I believe the following changes will allow us to quickly get the app in a releasable state and start testing:

# * Remove the "Cancel Res" column header

# * Make the GridSelector default option text a token.

# * When no grid is selected show a different message token to let the user know they need to select a grid (maybe something like "Select a time range to see your reservations.")

# * Your modal contains a lot of hard-coded text and there are a few UI changes I'd recommend before release. In the interest of time I'd like you to shelf the Modal code for now and use a JavaScript "confirm" dialog instead. Make sure the modal code is still in the app so we can wire it back in in the future.

# * When a reservation is being cancelled add some feedback so the user knows it's processing. This could be as simple as changing the button text to "cancelling..." or showing a "Processing cancellation" message on the page.

# * I may have missed some instances of hard-coded text. Go through the reservations tab and ensure all text is token-based.


# ]] ReservationsTab

# * Use React instead of jQuery to show/hide things when the grid is loading. This will simplify your code. I've made this change for you - here are updated getInitialState, handleGridChange and render methods. I've added a new "gridIsLoading" state variable. 

  getInitialState: ->
    windowWidth: window.innerWidth
    noRecords: true
    gdfFile: ""
    gridIsLoading: false

  handleGridChange: (grid) ->
    @setState gdfFile: grid, gridIsLoading: true
    jQuery.get @props.grid_info_path,
      {'grid_info': grid},
      (data) =>
        @setState {
                    grid: data.selected_grid,
                    gridIsLoading: false,
                    noRecords: (data.selected_grid.records.length == 0) }

  render: ->
    if @state.noRecords && !@state.gridIsLoading
      gridContent = <NoRecordMsg
                      noRecordsMsg={@props.no_records_msg}
                      />

    else unless @state.gridIsLoading
      gridContent = <ResGrid
                      cancelResPath={@props.cancel_res_path}
                       gdfFile={@state.gdfFile}
                       newGrid={@newGrid}
                       grid={@state.grid}
                       windowWidth={@state.windowWidth}
                       noRecordsMsg={@props.no_records_msg}
                       />

    if @state.gridIsLoading
      loader = <div id="loader">
          <i className="fa fa-spinner fa-pulse fa-3x" id='grid-spinner'></i>
        </div>

    gridSelector =  <GridSelector
                      key="gridSelector"
                      handleGridChange={@handleGridChange}
                      availableFiles={@props.available_files}
                      />

    divStyle = {marginBottom: 25};

    <div className="ReservationsTab">
      <div className="w3-col l12 w3-container">
        {gridSelector}
      </div>
      <div className="w3-col l12 w3-container">
        {loader}
        <div style={divStyle}>
         {gridContent}
        </div>
      </div>
    </div>



# current res_tab code
@ReservationsTab = React.createClass
  displayName: "ReservationsTab"

  getInitialState: ->
    windowWidth: window.innerWidth
    noRecords: true
    gdfFile: ""
    gridIsLoading: false

  handleResize: (e) ->
    @setState windowWidth: window.innerWidth

  componentDidMount: ->
    window.addEventListener('resize', @handleResize)

  componentWillUnmount: ->
    window.removeEventListener('resize', @handleResize)

  newGrid: (grid) ->
    @setState grid: grid


  handleGridChange: (grid) ->
    @setState gdfFile: grid, gridIsLoading: true
    jQuery.get @props.grid_info_path,
      {'grid_info': grid},
      (data) =>
        @setState {
                    grid: data.selected_grid,
                    gridIsLoading: false,
                    noRecords: (data.selected_grid.records.length == 0) }


  render: ->
    if @state.noRecords && !@state.gridIsLoading
      gridContent = <NoRecordMsg
                      noRecordsMsg={@props.no_records_msg}
                      />

    else unless @state.gridIsLoading
      gridContent = <ResGrid
                      cancelResPath={@props.cancel_res_path}
                       gdfFile={@state.gdfFile}
                       newGrid={@newGrid}
                       grid={@state.grid}
                       windowWidth={@state.windowWidth}
                       noRecordsMsg={@props.no_records_msg}
                       />

    if @state.gridIsLoading
      loader = <div id="loader">
          <i className="fa fa-spinner fa-pulse fa-3x" id='grid-spinner'></i>
        </div>

    gridSelector =  <GridSelector
                      key="gridSelector"
                      handleGridChange={@handleGridChange}
                      availableFiles={@props.available_files}
                      />

    divStyle = {marginBottom: 25};

    <div className="ReservationsTab">
      <div className="w3-col l12 w3-container">
        {gridSelector}
      </div>
      <div className="w3-col l12 w3-container">
        {loader}
        <div style={divStyle}>
         {gridContent}
        </div>
      </div>
    </div>    


# ]] CancelButton

# * When the cancel button is disabled (today's date is after the PickupTOD) I can still click it and get the cancel dialog. I'd rather you hide the button (and column) altogether instead of showing it disabled.

# ]] GridTable.Row

# * The loops you're using to build the table are inefficient. For each column you are looping through the entire record. It would be more efficient to use the following code. (Also, this code fixes your "flatten children" error. The problem was you were setting every record cell's key to the record ID.):

    tableData =
      for column in @props.grid.columns when !column.isHidden
        value = @props.record[column.fieldName]
        key = "#{@props.record.ID}-#{column.fieldName}"
        <GridTable.Record key={key} columnName={column.name} record={value}/>



# ]] environments/development.rb

# * I recommend changing config.assets.debug to "true" unless you have a reason to keep it on false. The difference in development load time between "false" and "true" is about 20 seconds vs 2 seconds per page.
