  openModal: (event) ->
    @setState isModalOpen: true
    @setState currentRecord: jQuery(event.target).data('record')

  closeModal: ->
    @setState isModalOpen: false
    @setState cancelRequestSent: false
    @setState requestError: false