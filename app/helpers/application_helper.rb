require 'enigma'
require 'countries'

module ApplicationHelper
  private

    def contacts_enabled?
      result = token('PROFILE_ENABLE_MY_CONTACTS')
      correct_site_version = SiteInfo.numeric_version(current_site_id) >= 3.35

      unless correct_site_version
        Rails.logger.warn '>> Not displaying contacts tab -- ' \
          'site needs to be upgraded.'
      end

      correct_site_version && !result.to_i.zero?
    end

    def locations_enabled?
      result = token('PROFILE_DISABLE_MY_LOCATIONS')
      result.to_i.zero? || result.blank?
    end

    def new_profile?
      params[:new_profile] || session[:new_profile]
    end

    def points_enabled?
      !@profile.programs.empty?
    end

    def profile_primary_background_color
      token('S0ROWCOLOR')
    end

    def profile_primary_text_color
      token('S0ROWTEXTCOLOR')
    end

    def reservations_enabled?
      disable = token('PROFILE_DISABLE_MYRES')
      disable.to_i.zero? || disable.blank?
    end

    def profile_secondary_background_color
      token('SEC_BG')
    end

    def profile_secondary_text_color
      token('SEC_TEXT')
    end

    def profile_site_background_color
      token('CSSBGCOLOR')
    end

    def profile_site_text_color
      token('CSSFONTCOLOR')
    end

    def profile_subsection_background_color
      token('SUB_BG')
    end

    def profile_subsection_text_color
      token('SUB_TEXT')
    end

    def alt_row_color
      color = Paleta::Color.new(:hex, primary_background_color.gsub(/#/, ''))
      "#" + "#{color.lighten!(25).hex}"
    end




    # def tab(label, url, selected = false)
    #   html =  "<li "
    #   html += "class='selected'" if selected
    #   html += "><a href='#{url}'><em>#{label}</em></a></li>"
    #   html.html_safe
    # end


    def tab(label, url, icon, selected = false)
      html = "<li class='nav-item'>"
      html += "<a class='nav-link #{'selected' if selected }' href='#{url}'>"
      html += "<div class='nav-group'>"
      html += "<div class= 'nav-group-icon'><i class='fa #{icon}'></i></div>"
      html += "<div class='nav-group-text'>#{label}</div>"
      html += "</div>"
      html += "</a>"
      html += "</li>"
      html.html_safe
    end

    def tab_icon(tab)
      label = case tab
      when :message then token('MESSAGE_HEADER_ICON').blank? ? 'fa-comment' : token('MESSAGE_HEADER_ICON')
      when :reservations then token('RESERVATIONS_HEADER_ICON').blank? ? 'fa-list' : token('RESERVATIONS_HEADER_ICON')
      when :profile then token('PROFILE_HEADER_ICON').blank? ? 'fa-users' : token('PROFILE_HEADER_ICON')
      when :locations then token('LOCATIONS_HEADER_ICON').blank? ? 'fa-map-marker' : token('LOCATIONS_HEADER_ICON')
      when :points then token('POINTS_HEADER_ICON').blank? ? 'fa-trophy' : token('POINTS_HEADER_ICON')
      when :contacts then token('CONTACTS_HEADER_ICON').blank? ? 'fa-user' : token('CONTACTS_HEADER_ICON')
      end
      label.html_safe
    end

    def tab_label(tab)
      label = case tab
      when :message then token('PROFILE_CAP_TAB_MESSAGE').blank? ? 'Message' : token('PROFILE_CAP_TAB_MESSAGE')
      when :reservations then token('PROFILE_CAP_TAB_RES').blank? ? 'Reservations' : token('PROFILE_CAP_TAB_RES')
      when :profile then token('PROFILE_CAP_TAB_PROFILE').blank? ? 'Account' : token('PROFILE_CAP_TAB_PROFILE')
      when :locations then token('PROFILE_CAP_TAB_LOCS').blank? ? 'Locations' : token('PROFILE_CAP_TAB_LOCS')
      when :points then token('PROFILE_CAP_TAB_POINTS').blank? ? 'Points' : token('PROFILE_CAP_TAB_POINTS')
      when :contacts then token('PROFILE_CAP_TAB_CONTACTS').blank? ? 'Contacts' : token('PROFILE_CAP_TAB_CONTACTS')
      end
      label.html_safe
    end

    def token(name, options = {})
      val = Tokens.for(@site_id, name, :GroupAlias => (@profile.ParentID rescue nil))
      val.blank? ? options[:default].to_s : val
    end
end
