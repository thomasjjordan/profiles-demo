module PointsHelper
  def profile_points_tab

    react_component 'PointsTab',
                    tab_header: points_header,
                    submit_url: points_path,
                    current_program_id: @current_program_id,
                    programs: @programs
  end

  def points_header
    token('PROFILE_DESC_POINTS').html_safe
  end


  def program_label
    token('CAP_PROFILE_PROGRAM').html_safe
  end

  def translate_dynamic_tokens(string)
    if string =~ /%PROFILE_LINK_GO_REWARDS%/
      rewards_hash = Digest::MD5.hexdigest("#{@profile.WebID}#{Time.now.gmtime.strftime('%Y/%m/%d')}Salt_Value_01")
      output = <<EOF
  		  <p style="margin-top: 15px;">
  		    <a target="_blank"
  		      href="http://gnet.infinitynetworkrewards.com/gnet/gologin.php?hash=#{rewards_hash}&profileID=#{@profile.WebID}" >Use Your Rewards</a>
  		  </p>
EOF
      output_string = string.gsub(/%PROFILE_LINK_GO_REWARDS%/, output)
    end

    (output_string || string).html_safe
  end
end
