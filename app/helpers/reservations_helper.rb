require 'base64'

module ReservationsHelper
  def profile_reservation_tab
    available_files = GDF.available_files(current_site_id, configured_desktop)
    site_version = SiteInfo.numeric_version(current_site_id)

    react_component 'ReservationsTab',
                    available_files: available_files,
                    details_caption: res_details_caption,
                    edit_back_caption: res_edit_back_caption,
                    get_res_edit_url: get_res_edit_address_url,
                    grid_info_path: grid_info_path,
                    no_grid_records_msg: no_grid_records_msg,
                    no_grid_selected_msg: no_grid_selected_msg,
                    grid_select_text: grid_select_text,
                    grid_select_label: grid_select_label,
                    grid_update_msg: grid_update_msg,
                    show_details_button: (site_version >= 3.45)
  end

  def configured_desktop
    desktop = token('PROFILE_CONFIGURED_DESKTOP')
    desktop.blank? ? 'Profiles' :  desktop
  end

  def no_grid_records_msg
    msg = token('PROFILE_NO_GRID_RECORDS_MSG')
    msg.blank? ? 'The selected grid has no records to show.' : msg
  end

  def no_grid_selected_msg
    msg = token('PROFILE_NO_GRID_SELECTED_MSG')
    msg.blank? ? 'Choose a selection to view your reservations.' :  msg
  end

  def grid_select_text
    msg = token('PROFILE_GRID_SELECT_TEXT')
    msg = "Select a time range" if msg.blank?
    msg
  end

  def grid_update_msg
    msg = token('PROFILE_UPDATE_GRID_MSG')
    msg = "Canceling Reservation" if msg.blank?
    msg
  end

  def grid_select_label
    msg = token('PROFILE_GRID_SELECT_LABEL')
    msg = "Showing" if msg.blank?
    msg
  end

  def iframe_tag(src)
    tag :iframe, scrolling: 'no', allowtransparency: 'true', marginwidth: '0',
        marginheight: '0', vspace: '0', hspace: '0', width: '768',
        src: src, frameborder: '0', height: '580'
  end

  def print_button_enabled?
    disabled = Configuration.for(current_site_id).token('PROFILE_DISABLE_RECEIPT')
    disabled.blank?
  end

  def print_reservation_url(wresid)
    query = "PID=" + "PRINTRESA=Y&RESNUMBER=#{wresid}".encrypted_for_res
    "/cgi-bin/#{current_site_id}/res?" + query
  end

  def res_details_caption
    msg = token('PROFILE_GRID_RES_DETAILS_LABEL')
    msg = 'Details' if msg.blank?
    msg
  end

  def res_edit_back_caption
    msg = token('PROFILE_GRID_RES_EDIT_BACK_LABEL')
    msg = 'Back to reservations' if msg.blank?
    msg
  end

  def view_button(id)
    link_to(image_tag('zoom.png', alt: 'View'), reservation_path(id: id), title: 'View')
  end
end
