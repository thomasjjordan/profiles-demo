module UserHelper

  def login_section_description
    token('LOGIN_SECTION_DESCRIPTION').blank? ? "Use your email address to sign into your account. Not registered?
    You'll speed your booking process next time by creating an account and providing us with some basic contact
    information." : token('LOGIN_SECTION_DESCRIPTION').blank?
  end

  def forgot_pw_header
    (token('PROFILE_FORGOTPW_HEADER') || 'Forgot My Password').html_safe
  end

  def login_header
    token('PROFILE_LOGIN_HEADER', default: 'Profile Login').html_safe
  end

  def login_section_header
   token('LOGIN_SECTION_HEADER').blank? ? 'Preferred Account Login' : token('LOGIN_SECTION_HEADER').blank?
  end

  def forgot_pw_section_header
   token('PROFILE_FORGOTPW_SECTION_HEADER').blank? ? 'Retrieve Password' : token('PROFILE_FORGOTPW_SECTION_HEADER').blank?
  end

  def login_username_label
   token('LOGIN_USERNAME_LABEL').blank? ? 'Username' : token('LOGIN_USERNAME_LABEL').blank?
  end

  def login_password_label
   token('LOGIN_PASSWORD_LABEL').blank? ? 'Password' : token('LOGIN_PASSWORD_LABEL').blank?
  end

  def login_button_text
   token('LOGIN_BUTTON_TEXT').blank? ? 'Login' : token('LOGIN_BUTTON_TEXT').blank?
  end

  def logging_in_text
   token('LOGGING_IN_TEXT').blank? ? 'Logging In...' : token('LOGGING_IN_TEXT').blank?
  end

  def forgot_pw_link_text
   token('FGT_PW_LINK_TEXT').blank? ? 'Retrieve Password' : token('FGT_PW_LINK_TEXT').blank?
  end

  def signup_link_text
   token('SIGNUP_LINK_TEXT').blank? ? 'Sign Up' : token('SIGNUP_LINK_TEXT').blank?
  end

  def login_link_text
   token('LOGIN_LINK_TEXT').blank? ? 'Login' : token('LOGIN_LINK_TEXT').blank?
  end

  def fgt_pw_button_text
   token('FORGOT_PW_BUTTON_TEXT').blank? ? 'Retrieve Password' : token('FORGOT_PW_BUTTON_TEXT').blank?
  end

  def fgt_pw_description_text
   token('FORGOT_PW_DESC_TEXT').blank? ? 'Your password will be emailed to you.' : token('FORGOT_PW_DESC_TEXT').blank?
  end

end
