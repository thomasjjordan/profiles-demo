module ContactHelper

  def contact_error(contact, field)
    (flash[:error_fields] && flash[:error_fields].include?(field)) ?
        'field_error' : ''
  end

  def contact_name_label
    token('CONTACT_CAP_NAME').html_safe
  end

  def contact_address_label
    token('CONTACT_CAP_ADDRESS').html_safe
  end

  def contact_email_label
    token('CONTACT_CAP_EMAIL').html_safe
  end

  def contact_city_label
    token('CONTACT_CAP_CITY').html_safe
  end

  def contact_state_label
    token('CONTACT_CAP_STATE').html_safe
  end

  def contact_zip_label
    token('CONTACT_CAP_ZIP').html_safe
  end

  def contact_telephone_label
    token('CONTACT_CAP_TELEPHONE').html_safe
  end

  def contact_comments_label
    token('CONTACT_CAP_COMMENTS').html_safe
  end

  def contact_authorized_label
    token('CONTACT_CAP_AUTHORIZED').html_safe
  end

  def contact_contact_type_label
    token('CONTACT_CAP_CONTACT_TYPE').html_safe
  end

  def contact_alt_telephone_label
    token('CONTACT_CAP_ALT_TELEPHONE').html_safe
  end

  # def contact_personal_header
  #   token('CONTACT_CAP_SECT_PERSONAL').html_safe
  #     if token('CONTACT_CAP_SECT_PERSONAL').blank?
  #       contact_personal_header = 'Contact Personal Information'
  #     end
  # end

  def successful_save_message
    token('CONTACT_CAP_SAVE_MESSAGE').html_safe
  end

  def contact_index_header
    token_value = token('CONTACT_CAP_INDEX_HEADER').html_safe
    token_value = "Contacts" if token_value.blank?
    token_value
  end

  def contact_personal_header
    token_value = token('CONTACT_CAP_SECT_PERSONAL').html_safe
    token_value = "Contact Personal Information" if token_value.blank?
    token_value
  end

  def contact_address_header
    token_value = token('CONTACT_CAP_SECT_ADDRESS').html_safe
    token_value = "Address" if token_value.blank?
    token_value
  end

  def contact_detail_header
    token_value = token('CONTACT_CAP_SECT_DETAIL').html_safe
    token_value = "Details" if token_value.blank?
    token_value
  end

  def contact_comments_header
    token_value = token('CONTACT_CAP_SECT_COMMENT').html_safe
    token_value = "Comments" if token_value.blank?
    token_value
  end

  def no_contact_present_message
    token('CONTACT_CAP_NO_CONTACTS').html_safe
  end

  def create_button_label
    token('CONTACT_CAP_CREATE_BUTTON').html_safe
  end

  def update_button_label
    token('CONTACT_CAP_UPDATE_BUTTON').html_safe
  end

  def name_label
    token('CONTACT_CAP_NAME').html_safe
  end

  def title_label
    token('CONTACT_CAP_TITLE').html_safe
  end

  def email_label
    token('CONTACT_CAP_EMAIL').html_safe
  end

  def telephone_label
    token('CONTACT_CAP_TELEPHONE').html_safe
  end

  def alt_telephone_label
    token('CONTACT_CAP_ALT_TELEPHONE').html_safe
  end

  def contact_fax_label
    token('CONTACT_CAP_CONTACT_FAX').html_safe
  end

  def comment_label
    token('CONTACT_CAP_COMMENTS').html_safe
  end

  def contact_country_label
    token('CONTACT_CAP_COUNTRY').html_safe
  end

  def address_label
    token('CONTACT_CAP_ADDRESS').html_safe
  end

  def city_label
    token('CONTACT_CAP_CITY').html_safe
  end

  def state_label
    token('CONTACT_CAP_STATE').html_safe
  end

  def zip_code_label
    token('CONTACT_CAP_ZIP_CODE').html_safe
  end

  def contact_type_label
    token_value = token('CONTACT_CAP_CONTACT_TYPE').html_safe
    token_value = "Contact Type" if token_value.blank?
    token_value
  end

  def authorized_label
    token('CONTACT_CAP_AUTHORIZED').html_safe
  end

  def use_usphone_format?
    token('VALIDATEPHONE').to_i == 1
  end

  def contact_types_token
    token('CONTACT_TYPES')
  end


  def display_name?
    !name_label.blank?
  end

  def display_title?
    !title_label.blank?
  end

  def display_email?
    !email_label.blank?
  end

  def display_telephone?
    !telephone_label.blank?
  end

  def display_alt_telephone?
    !alt_telephone_label.blank?
  end

  def display_contact_fax?
    !contact_fax_label.blank?
  end

  def display_comment?
    !comment_label.blank?
  end

  def display_address?
    !address_label.blank?
  end

  def display_contact_country?
    !contact_country_label.blank?
  end

  def display_city?
    !city_label.blank?
  end

  def display_state?
    !state_label.blank?
  end

  def display_zip_code?
    !zip_code_label.blank?
  end

  def display_contact_type?
    !contact_type_label.blank?
  end

  def display_authorized?
    !authorized_label.blank?
  end

  def contact_types?
    !token('CONTACT_TYPES').blank?
  end

  def display_personal_section?
    display_name? || display_email? || display_telephone? || display_alt_telephone?
  end

  def display_comment_section?
    display_comment?
  end

  def display_address_section?
    display_address? || display_city? || display_state? || display_zip_code?
  end

  def display_details_section?
    display_contact_type? || display_authorized?
  end

  def display_add_contact_button?
    @profile.MaxContacts.to_i == 0 || @contacts.count < @profile.MaxContacts.to_i
  end

  def contact_type_select?
    @contacts.new_record?
  end

  def formatted_phone(phone_in)
    use_usphone_format? ? phone_in.to_s.format_phone : phone_in
    if use_usphone_format?
    else

    end
  end

end

