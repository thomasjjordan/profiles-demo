module LocationsHelper
  def add_location_link_caption
    label = token('PROFILE_CAP_ADD_LOCATION')
    label = "Add a preferred location" if label.blank?
    label.html_safe
  end

  def address_label
    token('ROADDRESSDESC').html_safe
  end

  def address_tab_label
    label = token('PICKUPINFO_LOCTAB2')
    label = 'Address' if label.blank?
    label.html_safe
  end

  def available_addresses(address_list)
    options = [['-- Select an address --', '']]

		if address_lookup_type == :afd
	    address_list.each {|addr| options << [addr.template, addr.post_key]}
	  else
	    address_list.each do |addr|
	    	options << [
	    		"#{addr.street}, #{addr.city}, #{addr.state} #{addr.zip_code}",
	    		CGI.escape(Marshal::dump(addr))
	    	]
	    end
	  end
    options_for_select(options).html_safe
  end

  def city_label
    token('CITYDESC').html_safe
  end

  def cross_street_label
    label = token('ADDRNOTESDESC')
    label = 'Directions / Cross Street' if label.blank?
    label.html_safe
  end

  def display_search_address?
    displayed_address_fields.include? 'Address'
  end

  def display_search_city?
    displayed_address_fields.include? 'City'
  end

  def display_search_state?
    displayed_address_fields.include? 'State'
  end

  def display_search_zip?
    displayed_address_fields.include? 'ZipCode'
  end

  def displayed_address_fields
    fields = token('PICKUPINFO_LOCCAPTIONS').split(',')
		if fields.empty?
			fields = address_lookup_type == :afd ? %w(ZipCode) : %w(Address City State ZipCode)
		end
    fields
  end

  def display_address_lookup_tab?(airport_code)
    airport = Airports.airport_for(@site_id, airport_code)
    airport && airport.address_lookup_enabled?
  end

  def disable_initial_fields(pud)
    return nil if pud.FareCode.blank?
    fare = Locations.for_key(@site_id, pud.Airport, pud.FareCode)
    enable_disable_loc_fields(fare)
  end

  def enable_disable_loc_fields(fare)
    if fare.nil?
      output = "disableField('Address'); disableField('Zipcode'); disableField('City'); disableField('State');"
    else
      output = ''
      output << "#{fare.readonly_address? ? 'disable' : 'enable'}Field('Address');"
      output << "#{fare.readonly_zip? ? 'disable' : 'enable'}Field('Zipcode');"
      output << "#{fare.readonly_city? ? 'disable' : 'enable'}Field('City');"
      output << "#{fare.readonly_state? ? 'disable' : 'enable'}Field('State');"
    end

    output
  end

  def fill_in_location_fields(page, fare)
    # Fill in address/zip information
    page << "$('pud_Address').value = '#{fare.Address}';"
    page << "$('pud_Zipcode').value = '#{fare.Zip}';"
    page << "$('pud_Telephone').value = '#{fare.TelephoneNum}';"
    page << "$('pud_City').value = '#{fare.City}';"
    page << "$('pud_State').value = '#{fare.State}';"
  end

  def landmarks_tab_label
    label = token('PICKUPINFO_LOCTAB1')
    label = 'Landmarks' if label.blank?
    label.html_safe
  end

  def locations_header
    token('PUDINFO').html_safe
  end

  def pud_field(pud, type, field_options)
    caption = self.send("#{type.to_s}_label")
    displayed = !caption.blank?
    field_name = "pud[#{type.to_s.capitalize}]"
    hidden_field_id = "pud_#{type.to_s.capitalize}_hidden"
    value = pud.send(type.to_s.capitalize)

    output = ''
    if displayed
      output << %{<div class="left field_container" id="#{type}_field_container">}
      output << label_tag(field_name, caption)
      output << text_field_tag(field_name, value, field_options.merge(:class => 'text'))
      output << hidden_field_tag(field_name, value, :id => hidden_field_id)
      output << %{</div>}

    else
      output << hidden_field_tag(field_name, value)
      output << hidden_field_tag(field_name, value, :id => hidden_field_id)
    end

    output.html_safe
  end

  def pud_label
    label = token('PREFPICKUPDESC')
    label = 'Preferred Locations' if label.blank?
    label.html_safe
  end

  def pud_name(pud)
    pud.name.html_safe
  end

  def readonly_address?
  	@readonly['Address'] rescue false
  end

  def readonly_zip?
	  @readonly['Zip'] rescue false
  end

  def state_label
    token('STATEDESC').html_safe
  end

  def telephone_label
    label = token('TELEPHONEDESC')
    label = 'Telephone Number' if label.blank?
    label.html_safe
  end

  def zipcode_label
    token('PICKUPZIPDESC').html_safe

  end
end
