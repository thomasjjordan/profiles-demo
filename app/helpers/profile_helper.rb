module ProfileHelper

  def profile_text_input(profile, input_label, input_type)
    react_component 'ProfileInput',
                    inputValue: profile.instance_variable_get("@#{input_type}"),
                    label: profile_field_label(profile, :secondary, input_type, input_label),
                    inputType: input_type,
                    errorClass: error_class(profile, input_type)
  end

  def profile_masked_cc_input(profile, input_label, input_type)
    react_component 'ProfileInput',
                    inputValue: profile.masked_cc_number,
                    label: profile_field_label(profile, :secondary, input_type, input_label),
                    inputType: input_type,
                    errorClass: error_class(profile, input_type)
  end

  def profile_phone_input(profile, input_label, input_type)
    profile_phone = profile.instance_variable_get("@#{input_type}")
    react_component 'ProfileInput',
                    inputValue: formatted_phone(profile_phone),
                    label: profile_field_label(profile, :secondary, input_type, input_label),
                    inputType: input_type,
                    errorClass: error_class(profile, input_type)
  end

  def profile_password_input(profile, input_label, input_type)
    react_component 'ProfilePasswordInput',
                    inputValue: profile.masked_password,
                    label: profile_field_label(profile, :secondary, input_type, input_label),
                    inputType: input_type,
                    errorClass: error_class(profile, input_type)
  end

  def profile_section(section_header, section_description)
    react_component 'ProfileSection',
                    section_header: section_header,
                    section_description: section_description
  end

  def profile_submit_button(button_text, profile_submit_url)
    react_component 'ProfileSubmit',
                    button_text: button_text,
                    profile_submit_url: profile_submit_url
  end

  # **** react helpers end *****

  def personal_section_description
    token('PERSONAL_SECTION_DESCRIPTION').blank? ? 'Speed your booking process next time by
     providing us with some basic contact information. Your email address will serve as your
     Login Name.' : token('PERSONAL_SECTION_DESCRIPTION').blank?
  end

  def address_section_description
    token('ADDRESS_SECTION_DESCRIPTION').blank? ? 'We will save you time on your next visit
    and automatically complete your address details.' : token('ADDRESS_SECTION_DESCRIPTION').blank?
  end

  def password_section_description
    token('PASSWORD_SECTION_DESCRIPTION').blank? ? 'Protect your online account with a secure password.
   Use something that is easy for you to remember but difficult for others to guess.' : token('PASSWORD_SECTION_DESCRIPTION').blank?
  end

  def cc_section_description
    token('CC_SECTION_DESCRIPTION').blank? ? 'Your payment preference is encrypted and stored securely.
    Eliminate the need to enter payment information for every booking. ' : token('CC_SECTION_DESCRIPTION').blank?
  end

  def address_header
    token('PROFILE_SECT_ADDRESS').html_safe
  end

  def new_profile_header
    token('NEW_PROFILE_HEADER').blank? ? "New Preferred Account Profile" : token('NEW_PROFILE_HEADER')
  end

  def new_profile_submit_button_text
   token('NEW_PROFILE_SUBMIT_BTN_TEXT').blank? ? 'Register' : token('NEW_PROFILE_SUBMIT_BTN_TEXT')
  end

  def country_label
    token('PROFILE_CAP_COUNTRY').html_safe
  end

  def address_label
    token('PROFILE_CAP_ADDRESS').html_safe
  end

  def altphone_label
    token('PROFILE_CAP_ALTPHONE').html_safe
  end

  def cc_header
    token('PROFILE_SECT_CC').html_safe
  end

  def ccexp_label
    token('PROFILE_CAP_CCEXP').html_safe
  end

  def ccname_label
    token('PROFILE_CAP_CCNAME').html_safe
  end

  def ccnumber_label
    token('PROFILE_CAP_CCNUM').html_safe
  end

  def cczip_label
    token('PROFILE_CAP_CCZIP').html_safe
  end

  def city_label
    token('PROFILE_CAP_CITY').html_safe
  end

  def display_address?
    @profile.required_field?('Address') || !address_label.blank?
  end

  def display_country?
    @profile.required_field?('Country') || !country_label.blank?
  end

  def display_address_section?
    display_address? || display_city? || display_state? || display_zip?
  end

  def display_altphone?
    @profile.required_field?('Telephone2') || !altphone_label.blank?
  end

  def display_cc_section?
    display_ccname? || display_ccnumber? || display_ccexp? || display_cczip?
  end

  def display_ccexp?
    @profile.required_field?('CCExp') || !ccexp_label.blank?
  end

  def display_ccname?
    @profile.required_field?('CCDetails') || !ccname_label.blank?
  end

  def display_ccnumber?
    @profile.required_field?('CCNumber') || !ccnumber_label.blank?
  end

  def display_cczip?
    @profile.required_field?('CCDZipCode') || !cczip_label.blank?
  end

  def display_city?
    @profile.required_field?('City') || !city_label.blank?
  end

  def display_email?
    @profile.required_field?('EmailAddress') || !email_label.blank?
  end

  def display_name?
    @profile.required_field?('Name') || !name_label.blank?
  end

  def display_website?
    @profile.required_field?('WebSiteURL') || !website_label.blank?
  end

  def display_password?
    disable = token('PROFILE_DISABLE_PASSWORD')
    return false if disable.to_i == 1

    @profile.required_field?('WebPassword') || !password_label.blank?
  end

  def display_password_confirmation?
    @profile.required_field?('WebPasswordConfirm') || !password_confirmation_label.blank?
  end

  def display_password_section?
    display_password?
  end

  def display_personal_section?
    display_altphone? || display_name? || display_email? || display_phone?
  end

  def display_phone?
    @profile.required_field?('Telephone') || !phone_label.blank?
  end

  def display_fax?
    @profile.required_field?('FaxNumber') || !fax_label.blank?
  end

  def display_comments?
    @profile.required_field?('Comments') || !comments_label.blank?
  end

  def display_state?
    @profile.required_field?('State') || !state_label.blank?
  end

  def display_zip?
    @profile.required_field?('ZipCode') || !zip_label.blank?
  end

  def email_label
    token('PROFILE_CAP_EMAIL').html_safe
  end

  def error_class(profile, field)
    (profile && profile.error_fields && profile.error_fields.include?(field)) ?
      'field_error' : ''
  end

  def name_label
    token('PROFILE_CAP_NAME').html_safe
  end

  def website_label
    token('PROFILE_CAP_WEBSITE').html_safe
  end

  def password_header
    token('PROFILE_SECT_PASSWORD').html_safe
  end

  def password_label
    token('PROFILE_CAP_PASSWORD').html_safe
  end

  def password_confirmation_label
    token('PROFILE_CAP_PASSCONF').html_safe
  end

  def personal_header
    token('PROFILE_SECT_PERSONAL').html_safe
  end

  def phone_label
    token('PROFILE_CAP_PHONE').html_safe
  end

  def required_field?(profile, field)
    profile.error_fields && profile.error_fields.include?(field)
  end

  def profile_field_label(profile, type, field, label, id=nil)
    required_field = profile.required_field?(field)

    label = <<-EOF
  	  <label class="input-group-label w3-text-theme #{'description' if type == :primary} #{"error" if profile.error_fields && profile.error_fields.include?(field)} #{'required' if required_field}" id='#{id}'>
  	    #{label} #{' (Required)' if required_field}
  	  </label>
    EOF
    label.html_safe
  end

  def profile_page_header
   token('PROFILE_PAGE_HEADER').blank? ? 'My Profile' : token('PROFILE_PAGE_HEADER').blank?
  end

  def profile_site_background_color
    token('CSSBGCOLOR').blank? ? '#ffffff' : token('CSSBGCOLOR')
  end

  def profile_submit_button_text
   token('PROFILE_SUBMIT_BUTTON_TEXT').blank? ? 'Save Changes' : token('PROFILE_SUBMIT_BUTTON_TEXT').blank?
  end

  def show_right_column?
    display_password_section? || display_cc_section?
  end

  def state_label
    token('PROFILE_CAP_STATE').html_safe
  end

  def zip_label
    token('PROFILE_CAP_ZIP').html_safe
  end

  def use_usphone_format?
      token('VALIDATEPHONE').to_i == 1
  end

  def use_24hour_format?
      token('GEN_TIMESELFMT').to_i == 1
  end

  def formatted_time(time_in)
    use_24hour_format? ? time_in.strftime('%b %d %Y %H:%M') : time_in.strftime('%b %d %Y %I:%M %p')
  end

  def formatted_phone(phone_in)
     use_usphone_format? ? phone_in.to_s.format_phone : phone_in
  end

  def fax_label
    token('PROFILE_CAP_FAX').html_safe
  end

  def comments_label
    token('PROFILE_CAP_COMMENTS').html_safe
  end

end
