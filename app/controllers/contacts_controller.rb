class ContactsController < ApplicationController

  def index
    @contacts = Contact.find(@site_id, current_profile.ID)
  end

  def edit
    @contacts = Contact.find(@site_id, current_profile.ID, :ID => params[:id]).first
  end

  def save
    @contacts = Contact.find(@site_id, current_profile.ID, :ID => params[:id]).first
    params[:contact].each do |key, value|
      @contacts.send("#{key}=", value)
    end
    if @contacts.save
      redirect_to @contacts
      flash[:save_message] = token('CONTACT_CAP_SAVE_MESSAGE').html_safe
    else
      redirect_to :back
      flash[:error_fields] = @contacts.error_fields
      flash[:error]        = @contacts.save_errors
    end
  rescue WebService::ServiceError => e
    send_exception_report(e)
    @errors = ['We were unable to create your contact due to a system error.']
    redirect_to :back

    flash[:error]        = @errors

  rescue Exception => e
    flash[:error] = token('CONTACT_CAP_ERROR_MESSAGE').html_safe
    raise
  end

  def new
    @contacts = Contact.new(@site_id, current_profile.ID)
  end

  def delete
    @contact = Contact.find(@site_id, current_profile.ID, ID: params[:id]).first
    flash[:save_message] = 'The contact was deleted.' if @contact.try(:delete)
    redirect_to @contact

  rescue StandardError
    flash[:error] = 'The contact could not be deleted due to a system error.'
    redirect_to contacts_url
  end

  def create
    if @site_id.blank?
      redirect_to(login_url) and return
    end
    @contacts = Contact.new(@site_id, current_profile.ID)
    params[:contact].each do |key, value|
      @contacts.send("#{key}=", value)
    end
    if @contacts.save
      redirect_to @contacts
      flash[:save_message] = token('CONTACT_CAP_SAVE_MESSAGE').html_safe
    else
      redirect_to :back
      flash[:error_fields] = @contacts.error_fields
      flash[:error]        = @contacts.save_errors
    end
  rescue WebService::ServiceError => e
    send_exception_report(e)
    @errors = ['We were unable to create your contact due to a system error.']
    redirect_to :back
    flash[:error]        = @errors

  rescue Exception => e
    flash[:error] = token('CONTACT_CAP_ERROR_MESSAGE').html_safe
    raise
  end


  private
  def contact_email_locked?
    unless @contacts.new_record?
      locked = token('CONTACT_LOCK_EMAIL')
      locked.to_i == 1
    end
  end
  helper_method :contact_email_locked?
end
