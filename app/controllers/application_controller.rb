require 'enigma'
require 'base64'
require 'digest/md5'

class ApplicationController < ActionController::Base
  before_filter :read_encrypted_parameters
  before_action :check_for_bad_site
  before_filter :set_session, :except => 'clear'
  before_filter :login_profile, :except => 'clear'
  before_filter :change_tabs_if_necessary, :except => 'clear'
  before_filter :preload_tokens, :except => 'clear'
  before_action :allow_iframe_requests

  unless Rails.env.development?
    rescue_from Exception, with: :render_500
    rescue_from ActiveRecord::RecordNotFound,
                ActionController::RoutingError,
                ActionController::UnknownController,
                ::AbstractController::ActionNotFound, with: :render_500
  end

  def clear
    reset_session
    redirect_to(:action => 'login')
  end

  private

    def allow_iframe_requests
      response.headers.delete('X-Frame-Options')
    end

    def change_tabs_if_necessary
      redirect_to(:controller => params[:tab]) if params[:tab] && params[:controller] != params[:tab]
    end

    def check_for_bad_site
      if current_site_id.blank?
        render text: 'Missing site ID'
      else
        SiteServer.for(current_site_id)
      end
    rescue HudsonWebService::ServiceError => e
      if e.to_s =~ /Site not found/
        logger.error "Unable to find site for <#{current_site_id}>, displaying error."
        render text: 'Site not found'
      end
    end

    def current_profile
      @profile
    end

    def current_site_id
      if params[:site_id]
        session[:site_id] = params[:site_id]
      end
      @site_id = session[:site_id]
    end
    helper_method :current_site_id

    # If a profile id and password are passed in, try to log the user in
    def login_profile
      @clientcenter_request = params[:clientcenter]
      if params[:profile_id] && (params[:profile_pw] || @clientcenter_request)
        profile = Profiles.for(@site_id, params[:profile_id], :nocache => true)
        if profile &&
            ( profile_id_was_encrypted? ||
              (profile.WebPassword.blank? && @clientcenter_request) ||
              profile.check_password(params[:profile_pw]) )
          set_profile(profile)
          @profile = profile
        else
          redirect_to(login_url) && return
        end
      end
    end

    def preload_tokens
      token(%w(
        PROFILE_LOGIN_HEADER CSSBGCOLOR CSSFONTCOLOR PROFILE_DISABLE_MYRES PROFILE_CAP_TAB_RES
        PROFILE_CAP_TAB_PROFILE PROFILE_DISABLE_MY_LOCATIONS PROFILE_CAP_TAB_LOCS S0ROWCOLOR
        S0ROWTEXTCOLOR SEC_BG SEC_TEXT SUB_BG SUB_TEXT PREFPICKUPDESC PROFILE_PUD_DEFAULTAIRPORT
        PICKUPINFO_LOCTAB1 PICKUPINFO_LOCTAB2 PICKUPINFO_LOCCAPTIONS GEN_ADDRLISTSVC ROADDRESSDESC
        CITYDESC STATEDESC PICKUPZIPDESC TELEPHONEDESC ADDRNOTESDESC PROFILE_CAP_NAME PROFILE_CAP_EMAIL
        PROFILE_CAP_PHONE PROFILE_CAP_ALTPHONE PROFILE_CAP_ADDRESS PROFILE_CAP_CITY PROFILE_CAP_STATE
        PROFILE_CAP_ZIP PROFILE_CAP_PASSWORD PROFILE_CAP_PASSCONF PROFILE_CAP_CCNAME PROFILE_CAP_CCNUM
        PROFILE_CAP_CCEXP PROFILE_CAP_CCZIP PROFILE_SECT_PERSONAL PROFILE_SECT_ADDRESS PROFILE_SECT_PASSWORD
        PROFILE_SECT_CC VALIDATEPHONE
      ))
      # (Called without group alias))
      Tokens.for(@site_id, %w(PICKUPCITYDESC PICKUPCORPDESC PICKUPHOTELDESC PICKUPAIRPORTDESC
        PICKUPOTHERDESC PROFILE_PUD_LOCTYPES PUDINFO))
    end

    def profile_id_was_encrypted?
      !!@encrypted_profile_id
    end

    # Decrypt encrypted parameters.
    # If a PID parameter is passed in its expected to be enigma-encrypted, have the enigma key prepended, and then
    # the resulting string Base64-encrypted.
    # A ENC1 parameter is expected to be encrypted using aes-128-cbc with the key and IV stored in memcache.
    def read_encrypted_parameters
      if b64_encrypted = params[:PID]
        string = Base64.decode64(b64_encrypted.gsub(' ', '+'))
        if string =~ /([^-]+)-(.+)/
          parameters = $2.enigma_decrypt($1)
          params.update(Rack::Utils.parse_query(parameters))
        end

        # Account for defaults[] params
        params[:defaults] = {} if params[:defaults].nil?
        params.each do |key, value|
          logger.info("Key is #{key}")
          if key =~ /defaults\[(.*?)\]/
            params[:defaults][$1] = value
          end
        end
        params[:defaults] = nil if params[:defaults].empty?

      elsif aes_encrypted = params[:ENC1]
        aes_encrypted = URI.unescape(aes_encrypted)
        cache_key, ciphertext = aes_encrypted.split('.')

        # Retrieve key and IV
        dc = Dalli::Client.new('localhost:11211')
        unless strings = dc.get("Encrypt:profiles:#{cache_key}")
          render(text: "ERROR: Unable to parse parameter string.") && return
        end
        key, iv = strings.split(' | ')

        # Decrypt
        cipher = OpenSSL::Cipher::Cipher.new('aes-128-cbc')
        cipher.key = Base64.decode64(key)
        cipher.iv = Base64.decode64(iv)
        cryptogram = Base64.decode64(ciphertext)
        parameters = cipher.update(cryptogram)
        parameters << cipher.final

        encrypted_parameters = Rack::Utils.parse_query(parameters)
        params.update(encrypted_parameters)

        @encrypted_profile_id = encrypted_parameters.include?('profile_id')
      end

      if Rails.env.development? || Rails.env.demo?
        logger.info("Unencrypted parameters: #{params.inspect}")
      end
    end

    def reload_profile
      set_profile(Profiles.for(@site_id, @profile.ID, :nocache => true))
    end

    def require_profile_to_be_loaded
      unless @profile && @profile.existing_record?
        redirect_to login_url
        return false
      end
    end

    def replace_tokens
      replace_tokens_in_response(@site_id, (@profile.ParentID rescue nil))
    end

    def reservations_enabled?
      disable = token('PROFILE_DISABLE_MYRES')
      disable.to_i.zero? || disable.blank?
    end

    def render_500(exception)
      if exception.is_a?(ActionView::TemplateError) && exception.to_s =~ /in a list of known sites/
        render plain: "Invalid address. Please check your URL and try again."
      else
        send_exception_report(exception)
        render 'public/500.html'
      end
    end

    def send_exception_report(exception)
      Alarm.new(current_site_id, exception, request.env).ring
    end

    def set_profile(profile)
      session[:profiles] = {@site_id => profile}
    end

    def set_session
      current_site_id

      if session[:profiles].is_a?(Hash) and session[:profiles][@site_id]
        @profile = session[:profiles][@site_id]
      end

      # Res-edit options
      @resedit_hide_ok_button = true

      # Javascript scripting domain.
      session[:domain] = params[:domain] if params[:domain]
      @domain = session[:domain]
    end

    def token(name)
      # Tokens.for(@site_id, name, :GroupAlias => "173")
      Tokens.for(@site_id, name, :GroupAlias => (@profile.ParentID rescue nil))
    end
end
