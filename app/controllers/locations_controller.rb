class LocationsController < ApplicationController
  before_filter :require_profile_to_be_loaded
  after_filter :replace_tokens

	def bad_test
		bad_call
	end

  def address_lookup
    airport_code = params[:airport]
    if params[:zip].blank?
      error = "#{zip_label} is required."
    end

    if !error
      addresses = AddressLookup.for(current_site_id, :Address => params[:address],
          :City => params[:city], :ZipCode => params[:zip], :State => params[:state],
          :AirportCode => airport_code)
      error = "No results were found." if addresses.empty?
    end

    if error
      render json: { error: error }
    else
      render json: {
          content: render_to_string('locations/_address_lookup_results',
              layout: false, locals: { addresses: addresses }) }
    end
  rescue Exception => e
    render json: { error: e.to_s }
  end

  def index
    @selected_pud = params[:selected_pud] || @profile.puds.find_all{|p| p.individual?}.first.ID rescue nil
    @puds_error = params[:puds_error]

  rescue Exception => e
    flash[:error] = "The system is currently down, but will be back up shortly."
    raise
  end

  def location_info
    @location = Locations.for_key(@site_id, params[:fare_code], airport_code: params[:airport_code])
    render json: @location
  end

  # Return HTML for a PUD section.
  # Pass an "id" parameter to load an existing pud.
  # If no "id" then a new pud is returned.
  def pud
    if params[:id]
      pud = current_profile.puds.find {|pud| pud.ID.to_i == params[:id].to_i}
    else
      pud = PUD.new(current_site_id, current_profile, 'PUDAirport' => params[:airport])
    end
    render partial: 'pud', object: pud
  end

  def save_pud
    if params[:pud].blank?
      redirect_to(locations_url) && return
    end

    # Create a PUD with incoming info and save.
    new_id = PUD.new(@site_id, @profile, params[:pud]).save
    reload_profile

    selected_pud = params[:pud][:ID].blank? ? new_id : params[:pud][:ID]
    redirect_to locations_url(:selected_pud => selected_pud)
  rescue HudsonWebService::ServiceError => e
    redirect_to locations_url(:selected_pud => selected_pud, :puds_error => e.to_s)
  end

  def address_from_lookup
    result = params[:search_result]
    airport = params[:airport]

    if address_lookup_type == :afd
      address = AddressLookup.details(current_site_id, params[:search_result], params[:airport])
    else # map point
      address = Marshal::load(CGI.unescape(params[:search_result]))
    end
    render json: address
  end

  def update_pud_locations
    @locations = @profile.valid_locations(params[:airport_code])

    # Load fare information.
    @location = Locations.for_key(@site_id, @locations.first.FareCode, airport_code: params[:airport_code])

    respond_to do |format|
      format.js
    end
  end

  private
    def address_lookup_type
      service = token('GEN_ADDRLISTSVC')
      service = 1 if service.blank?
      service.to_i == 1 ? :map_point : :afd
    end
    helper_method :address_lookup_type

    def zip_label
      label = token('PICKUPZIPDESC')
      label = 'Zip Code' if label.blank?
      label
    end
end
