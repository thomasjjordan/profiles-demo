class ProfileController < ApplicationController
  layout :login_or_tabs

  before_filter :require_profile_to_be_loaded, :except => [ 'new', 'main']
  after_filter :replace_tokens, :only => ['profile', 'index', 'main', 'reservations', 'message', 'reservation']

  def index
    redirect_to :action => 'main' and return
  end

  def new
    if @site_id.blank?
      redirect_to(login_url) and return
    end
    @profile = nil if !request.post? # Make sure we're not overriding an existing profile
    new_incoming_profile = params[:defaults]

    # -- Massage incoming profile params
    prof_in = (request.post? and params[:profile]) ?
      params[:profile] :
      (params[:defaults] || {})

    # CCExp
    begin
      if params[:date].is_a?(Hash) and
          !params[:date][:year].blank? and
          !params[:date][:month].blank?
        prof_in[:CCExp] = Time.local(params[:date][:year], params[:date][:month])
      elsif !prof_in[:CCExp].blank?
        prof_in[:CCExp] = Time.parse(prof_in[:CCExp])
      elsif prof_in[:CCExp].blank?
        prof_in.delete(:CCExp)
      end
    rescue ArgumentError => e
      # If we got bad month/year data
      if e.to_s =~ /argument out of range/
        @errors = ['Invalid expiration date']
        return if request.post?
        prof_in.delete :CCExp
      else
        raise
      end
    end

    # Handle FirstName/LastName
    unless prof_in[:Name]
      prof_in[:Name] = (prof_in[:FirstName].blank?) ? '' : (prof_in[:FirstName] + ' ')
      prof_in[:Name] += prof_in[:LastName].to_s
    end

    # AdminID
    prof_in[:AdminID] = params[:admin_id] if params[:admin_id]

    if new_incoming_profile || !@profile
      @profile = Profile.new(@site_id, nil, prof_in)
      set_profile(@profile)
    else
      @profile.update(prof_in)
    end

    if request.post?
      if @profile.save
        # Add to mailing lists
        if params[:mailing_list]
          params[:mailing_list].each do |key, value|
            EmailSubscriptions.add(@site_id, key, @profile.EmailAddress, request.referer,
              request.env['HTTP_X_FORWARDED_FOR'] || request.remote_ip)
          end
        end

        set_profile(Profiles.for(@site_id, @profile.ID))
        redirect_to :action => 'profile', :new_profile => true
      else
        @errors = @profile.save_errors
      end
    end

    @email_subs = EmailSubscriptions.for(@site_id)

  rescue Exception => e
    send_exception_report(e)
    @errors = ['We were unable to create your profile due to a system error.']
  end

  def profile
    if params[:new_profile]
      session[:new_profile] = true
      redirect_to(:action => 'message') and return
    end

    if request.post? and params[:profile]
      # Update the profile.
      profile_params = params[:profile]

      # Don't allow changing email if locked
      profile_params.delete(:EmailAddress) if profile_email_locked?
      profile_params.delete(:WebPasswordConfirm) if profile_params[:WebPasswordConfirm] == @profile.masked_password
      profile_params.delete(:WebPassword) if profile_params[:WebPassword] == @profile.masked_password

      # CCExp
      begin
        if params[:date].is_a?(Hash) and
            !params[:date][:year].blank? and
            !params[:date][:month].blank?
          profile_params[:CCExp] = Time.local(params[:date][:year], params[:date][:month])
        end
      rescue ArgumentError => e
        # If we got bad month/year data
        if e.to_s =~ /argument out of range/
          @errors = ['Invalid expiration date']
          return
        else
          raise
        end
      end


      @profile.update(profile_params)

      if @profile.save
        @notice = token('PROFILE_UPDATED_MESSAGE')
        @notice = 'Your profile has been updated' if @notice.blank?
        reload_profile
      else
        @errors = @profile.save_errors
      end
    end

    render :action => 'profile'

  rescue Exception => e
    flash[:error] = "The system is currently down, but will be back up shortly."
    raise
  end

  def main
    redirect_to (reservations_enabled? ? reservations_url : profile_url)
  end

  private
    def profile_email_locked?
      locked = token('PROFILE_LOCK_EMAIL')
      locked.to_i == 1
    end
    helper_method :profile_email_locked?

    # Determine which layout to use.
    def login_or_tabs
      ['new'].include?(params[:action]) ? 'login' : 'application'
    end
end
