require 'json'
class PointsController < ApplicationController
  include PointsHelper
  before_filter :require_profile_to_be_loaded
  after_filter :replace_tokens

  def index
    # params.merge!({program: "4"}) unless params[:program].present?
    sorted_programs = @profile.programs.sort{|a,b| a.name <=> b.name}
    @current_program_id = params[:program] || sorted_programs.first.program_id
    @programs = sorted_programs.map {|program| program.as_json}

    #call translate_dynamic_tokens on program descriptions
    @programs.map do |program|
      selected_program = sorted_programs.select {|p| p.program_id == program['program_id']}
      program['description'] = translate_dynamic_tokens(selected_program[0].description)
    end

    if request.post?
      # Update program fields.
      if params[:update]
        @current_program = @profile.programs.find {|p| p.program_id == params[:program]}
        old_field_values = {}

        @current_program.fields.each do |field|
				# Backwards compatibility for people with older sessions. Safe to remove after a while.
				field = field[1] if field.is_a?(Array)

          old_field_values[field.name] = @current_program.send(field.name)
          @current_program.send("#{field.name}=", params[field.name])
        end

        begin
          @profile.update_program(@current_program)
          @notice = "Successfully updated your program information."
          @current_program.member = true
        rescue Exception => e
          @error = "We're temporarily unable to update your program information."
          logger.info("Exception: #{e.to_s}")
          send_exception_report(e)

          # I don't want to display the updated field values if they didn't save. Since they would
          # persist through the rest of the user's session the user may mistakenly think they saved.
          old_field_values.each {|field, value| @current_program.send("#{field}=", value) }
        end
      end
      @programs = sorted_programs.map {|program| program.as_json}
    end
    # render component: 'PointsTab', props: { program_list: @profile.methods, tab_header: points_header } , tag: 'span'
  rescue HudsonWebService::ObsolescenceError
    @transactions = []

  end

  def rewards_error
    @error = params['ERROR']
    render :action => 'rewards_error', :layout => 'login'
  end
end



