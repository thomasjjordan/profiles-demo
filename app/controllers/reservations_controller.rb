class ReservationsController < ApplicationController
  include ReservationsHelper
  before_filter :require_profile_to_be_loaded

  def index
    # desktops = Desktop.for(current_site_id)
    # desktops.map { |desktop|  Rails.logger.debug "\ndesktop.ViewAlias\n#{desktop.ViewAlias}\n" }
    # @my_gdf = GDF.available_files('eric', 'Profiles')
    #  Rails.logger.debug "\nmy_gdf\n#{@my_gdf}\n"

     # @my_gdf_file =  GDF.by_filename_and_profile('eric', 'VMDT/ReservationsCurrent.gdf', current_profile)
     # Rails.logger.debug "\nmy_gdf_file\n#{@my_gdf_file.inspect}\n"

    # Date Range
    session[:date_range] = params[:date_range] if params[:date_range]
    @date_range = session[:date_range].to_sym rescue nil

    @show_back_button = params[:show_back_button]
    @close = params[:close] if params[:close]

    # Get profile reservations.
    @reservations = nil
    unless @date_range.blank?
      begin
        # update to ask the profile if it's individual or not?
        if current_profile.is_individual?
          @reservations = Profile::Reservations.for(current_site_id, current_profile, @date_range)
        elsif @selected_member
          @reservations = Profile::Reservations.for(current_site_id, @selected_member, @date_range)
        else
          @reservations = Profile::Reservations.for_group_profile(current_site_id, current_profile, @date_range)
        end

      rescue HudsonWebService::ServiceError => e
        raise unless e.to_s =~ /User\s+not found/
      end
    else
      @reservations = []
    end

    @reservations_error = params[:reservations_error]

  rescue Profile::Res::MissingRequiredFields => e
    send_exception_report(e)

  rescue GdfRecords::GdfConfigError, HudsonWebService::ServiceError => e
    if e.to_s =~ /(error loading file)|(not configured for)/
      send_exception_report(e)
      @reservations_error = e.to_s
      @reservations = []
    else
      raise
    end

  rescue Exception => e
    flash[:error] = "The system is currently down, but will be back up shortly."
    raise
  end

  def res_edit_address
    host = Rails.env.development? ?
        "#{request.protocol}#{SiteServer.server_for(current_site_id)}" : ''
    uri = "#{host}/cgi-bin/#{current_site_id}/rwd"

    now = Time.now.to_i.to_s
    encrypted = "FED=##{params[:id]}&IFR=1".enigma_encrypt(now)
    encrypted = Base64.encode64("#{now}-#{encrypted}")
    render json: { url: "#{uri}?PID=#{encrypted}" }
  end

  def reservation
    unless params[:id]
      redirect_to reservations_url; return
    end

    # Don't allow viewing of a res that the user doesn't have.
    filter_string = ReadRes.filter_string(current_site_id, current_profile)
    res = ReadRes.find_by_id(current_site_id, params[:id], {}, filter_string)

    if res.blank?
      redirect_to reservations_url; return
    end
  rescue HudsonWebService::ServiceError
    logger.error "Reservation id could not be found associated with this user."
    redirect_to reservations_url; return
  end

  def grid_info
    grid_info = params[:grid_info]
    selected_grid = GDF.by_filename_and_profile(current_site_id, grid_info, current_profile)
    render json: {selected_grid: selected_grid.as_json}
  end

end
