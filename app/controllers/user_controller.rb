class UserController < ApplicationController
  layout 'login'
  before_filter :preload_tokens

  def login
    if @site_id.blank?
      render_500 && return
    end

    @notice ||= flash[:notice]
    @error = flash[:error]

    if request.post? and params[:username]
      @username = params[:username]
      if params[:password].blank?
        @error = "Password is required."
      else
        profile = Profiles.for(@site_id, @username)
        if !profile
          @error = "Invalid username."
        elsif !profile.check_password(params[:password])
          @error = "Invalid password."
        else
          set_profile(profile)
          if @login_only
            @close_window = true
					else
            redirect_to reservations_enabled? ? reservations_url : profile_url
          end
        end
      end
    end
  end

  def login_user
    return if @site_id.blank?
    @login_only = session[:login_only]

    if params[:username]
      @username = params[:username]
      profile = Profiles.for(@site_id, @username)
      if !profile
        @error = "Invalid username."
      elsif !profile.check_password(params[:password])
        @error = "Invalid password."
      else
        session[:new_profile] = false
        set_profile(profile)
        @profile_id = profile.ID
        @profile_pw = profile.WebPassword
      end
    end

    respond_to do |format|
      format.js
      format.html do
        if @error.present?
          flash[:error] = @error
          redirect_to(login_url)
        else
          redirect_to((reservations_enabled? ? reservations_url : profile_url))
        end
      end
    end
  end

  def logout
    site_id = @site_id
    reset_session
    redirect_to login_url(:site_id => site_id)
  end

  def forgot_my_password
    return if current_site_id.blank? || !params[:username]

    RecoverPassword.send_password_email(current_site_id, params[:username])
    redirect_to login_url, notice: "A password recovery email will be sent to you shortly."
  rescue RecoverPassword::Error => e
    @error = ['We were unable to recover your password at this time.']
  end
end
