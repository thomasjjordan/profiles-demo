#Alert the user if they try to navigate away without saving.
formHasChanged = false
submitted = false
jQuery(document).on "change keyup", ":input", (e) ->
  formHasChanged = true


jQuery(document).ready ->
  window.onbeforeunload = (e) ->
    if formHasChanged and not submitted
      message = "You have unsaved changes on this page. Do you want to leave this page and discard your changes or stay on this page?"
      e = e or window.event
      e.returnValue = message  if e
      message

  jQuery("form").submit ->
    submitted = true


