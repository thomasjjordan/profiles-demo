@ProgramInput = React.createClass
  displayName: "ProgramInput"

  getInitialState: ->
    fieldValue: @props.fieldValue
    isChecked: true

  handleInputChange: (event) ->
    @setState fieldValue: event.target.value

  handleCheckboxChange: ->
    checkedState = @state.isChecked
    @setState isChecked: !checkedState

  componentWillReceiveProps: (nextProps) ->
    @setState fieldValue: nextProps.fieldValue

  componentWillUpdate: ->
    jQuery('#program-input').removeClass "input-error"
    jQuery('#program-input-label').show()
    jQuery('.input-error-message').hide()

  render: ->
    className= "input-group-input w3-text-theme-border"
    className += " validation" if @props.validationRequired
    className += " required" if @props.fieldRequired
    if @props.fieldType == "T"
      <div>
        <label className="input-group-label w3-text-theme" id="program-input-label">
          {@props.fieldLabel}
        </label>
        <span className= "input-error-message"> Please provide a valid {@props.programName}</span>
        <input data-validation={@props.validationType} type="text" className={className} id={@props.fieldName}
         onChange={@handleInputChange} name={@props.fieldName}
         ref="program-input" value={@state.fieldValue}/>
      </div>
    else
      className= "required" if @props.fieldRequired
      <label className="w3-checkbox">
        <input className={className} onChange={@handleCheckboxChange} type="checkbox" name={@props.fieldName} checked={@state.isChecked} value={checkedValue = if @state.isChecked then 1 else 0}/>
        <div className="w3-checkmark"></div> {@props.fieldLabel}
      </label>

