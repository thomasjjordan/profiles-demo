@TransactionTable = React.createClass
  displayName: "TransactionTable"

  buildTable: (transactions) ->
    if transactions.length == 0
      <tr>
        <td> -- </td>
        <td> -- </td>
        <td> -- </td>
        <td className="table-align-right"> -- </td>
      </tr>
    else
      for transaction in transactions
        debitCredit = "-" if transaction.DebitCredit == "D"
        noComment = "--" if not transaction.Comments
        <tr key ={transaction.ID}>
          <td className= "trans-TOD">{transaction.TransTOD}</td>
          <td className= "trans-Name">{transaction.TransName}</td>
          <td className= "trans-Comments">{noComment}{transaction.Comments}</td>
          <td className= "trans-Points">{debitCredit}{transaction.Points}</td>
        </tr>

  render: ->
      <div className="transaction-table">
        <label className="input-group-label w3-text-theme" htmlFor="program-selector"></label>
        <table id="transactions">
          <thead>
            <tr>
              <th className ="w3-theme">Date</th>
              <th className ="w3-theme">Transaction</th>
              <th className ="w3-theme">Comments</th>
              <th className ="w3-theme">Points</th>
            </tr>
          </thead>
          <tbody>
            {@buildTable(@props.currentProgram.transactions)}
            <tr>
              <td className ="w3-theme" colSpan="3" id="current-total">Current Total:</td>
              <td className ="w3-theme" id="total-points">{@props.currentProgram.points}</td>
            </tr>
          </tbody>
        </table>
      </div>