@ProgramSelector = React.createClass
  displayName: "ProgramSelector"

  onProgramChange: (event) ->
    programId = event.target.value
    @props.updateProgram(programId)

  createProgramOptions: (program) ->
    <option key={program.program_id} value={program.program_id}>{program.name}</option>

  render: ->
    <div className="program-select-block">
      <label className="input-group-label w3-text-theme" htmlFor="program-selector">Select Program</label>
      <div className="input-group-select w3-text-theme-border" id="program-select">
        <div className="select-wrapper">
          <select value={@props.currentProgram.program_id} onChange={@onProgramChange} id="program-selector">{@props.programs.map @createProgramOptions}</select>
        </div>
      </div>
    </div>

