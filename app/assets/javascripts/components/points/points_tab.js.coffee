@PointsTab = React.createClass
  displayName: "PointsTab"

  getInitialState: ->
    currentProgram: @_program(@props.current_program_id)

  updateProgram: (programId) ->
    @setState currentProgram: @_program(programId)

  _program: (programId) ->
    for program in @props.programs when program.program_id == programId
      return program

  render: ->
    transactionTable =
      <TransactionTable currentProgram= {@state.currentProgram}/>

    <div className="w3-margin-bottom" id="points-content-window">
      <div className="w3-row">
        <div className="description-and-input-block">
          <div className="w3-col m12 l6 w3-container">
            <div className="profile-header-container">
              <h2 className="w3-text-theme-primary">{@props.tab_header}</h2>
            </div>
          </div>
        </div>
      </div>
      <div className="w3-row">
        <div className="description-and-input-block">
          <div className="w3-col m12 l5 w3-container">
            <ProgramSelector key ="programSelector" updateProgram= {@updateProgram} programs= {@props.programs} currentProgram= {@state.currentProgram}/>
            <Program currentProgram= {@state.currentProgram}/>
          </div>
          <div className="w3-col m12 l7 w3-container">
            <div className="input-block">
              <div className="input-group-container">
              {if @state.currentProgram.member then transactionTable}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

