@Program = React.createClass
  displayName: "Program"

  createDescription: ->
    { __html: @props.currentProgram.description}

  validate: (e) ->
    valid = true
    required_fields = true
    valid_aa = true
    valid_ua = true
    valid_vv = true
    blankRE = /^[\s]*$/
    {# Get list of textboxes to be validated.}
    requiredFields = jQuery('.required')
    for field in requiredFields
      if jQuery(field).is( "[type=text]" ) and blankRE.test(field.value)
        required_fields = false

      if jQuery(field).is( "[type=checkbox]" ) and !field.checked
        required_fields = false

    {# Get list of textboxes to be validated.}
    fields = jQuery('.validation')
    for field in fields
      {#American Airlines}
      if !field.value.length < 0 and jQuery(field).data('validation') == 2
        if !@valid_american_airlines(field.value)
          valid_aa = false
      {#United Airlines}
      else if !field.value.length < 0  and jQuery(field).data('validation') == 3
        if !@validUnitedAirlinesMP(field.value)
          valid_ua = false
      {#virgin Velocity}
      else if !field.value.length < 0  and jQuery(field).data('validation') == 4
        if !@valid_virgin_velocity(field.value)
          valid_vv = false
    if !required_fields or !valid_aa or !valid_ua or !valid_vv
      jQuery('#program-input').addClass "input-error"
      jQuery('#program-input-label').hide()
      jQuery('.input-error-message').show()
      e.preventDefault()

  valid_american_airlines: (id) ->
    id = id.toUpperCase().strip()
    {# Length is 7 characters.}
    if id.length != 7
      return false
    {# Always end in even number.}
    if id.charAt(id.length - 1) % 2 != 0
      return false
    {# The letters 'O', 'Q', 'Z', and 'I' are never used.}
    if ['O','Q','Z','I'].any(((c) ->
        id.include c
      ))
      return false
    {# Letters 'G' and 'S' will only appear in the first position}
    if ['G','S'].any(((c) ->
        id.substring(0).include c
      ))
      return false
    {# Validate the check digit.}
    if !@valid_american_airlines_check_digit(id)
      return false
    true

  valid_american_airlines_check_digit: (id) ->
    check_digit = parseInt(id.charAt(id.length - 1))
    calculated_check_digit = parseInt((@get_aa_digit(id, 1) + @get_aa_digit(id, 3) + @get_aa_digit(id, 3) + @get_aa_digit(id, 4) + @get_aa_digit(id, 5) + @get_aa_digit(id, 6)) / 5 * 10) % 10
    check_digit == calculated_check_digit

  get_aa_digit: (id, position) ->
    mychar = id.substring(position - 1, position)
    if mychar >= 'A' and mychar <= 'I'
      mychar.charCodeAt(0) - 'A'.charCodeAt(0) + 193
    else if mychar >= 'J' and mychar <= 'R'
      mychar.charCodeAt(0) - 'J'.charCodeAt(0) + 209
    else if mychar >= 'S' and mychar <= 'Z'
      mychar.charCodeAt(0) - 'S'.charCodeAt(0) + 226
    else if mychar >= 0 and mychar <= 9
      mychar.charCodeAt(0) - '0'.charCodeAt(0) + 240
    else
      -1

  valid_virgin_velocity: (id) ->
    {# The membership number is 10 digits. The first 9 digits are sequential. The last digit is a check digit, computed as modulo 7 of the sum of the first 9 digits.}
    if id.length != 10 or isNaN(id)
      return false
    i = undefined
    checksum = 0
    checkDigit = parseInt(id.charAt(9))
    i = 0
    while i < id.length - 1
      checksum += parseInt(id.charAt(i))
      i++
    if checksum % 7 == checkDigit
      return true
    false

  validUnitedAirlinesMP: (id) ->
    i = undefined
    thisChar = undefined
    digit = undefined
    checkDigit = undefined
    transcoder = 'AJ BKS CLT DMU ENV FOW GPX HQY IRZ '
    id = id.toUpperCase().strip()
    {# A valid id is 8 characters long.}
    {# The first 3 chars must be alphanumeric.}
    {# The last 5 chars must be numeric.}
    if !/[\dA-Z]{3}\d{5}/.test(id)
      return false
    {# Validate the number using maths.}
    checkDigit = 0
    i = 0
    while i < id.length - 1
      {# Cycle through the first 7 chars.}
      thisChar = id.charAt(i)
      {# Generate a digit if we have a letter.}
      if isNaN(thisChar)
        digit = Math.floor(transcoder.indexOf(thisChar) / 4 + 1)
      else
        digit = parseInt(thisChar)
      {# This digit gets added to the total.}
      if i % 2 == 0
        checkDigit += Math.floor(digit * 2 / 10 + digit * 2 % 10)
      else
        checkDigit += digit
      i++
    checkDigit = 10 - (Math.floor(checkDigit) % 10)
    if checkDigit != parseInt(id.charAt(7))
      checkDigit++
      if checkDigit == 10
        checkDigit = 1
      if checkDigit != parseInt(id.charAt(7))
        return false
    true

  render: ->
    buttonText = if @props.currentProgram.member then "Save Changes" else "Participate"
    fields = @props.currentProgram.fields.map (field) =>
      fieldLabel = field.label
      fieldLabel += " (Required)" if field.required > 0
      <ProgramInput programName={field.label}
                    validationType={field.validation_type}
                    fieldName={field.name}
                    fieldRequired={field.required}
                    validationRequired={field.validation_required}
                    fieldType ={field.type}
                    fieldLabel= {fieldLabel}
                    fieldValue={field.field_value}/>

    <div className="Program">
      <div className="description-container">
        <div dangerouslySetInnerHTML={@createDescription()} />
      </div>
      <form className="PointsForm" acceptCharset="UTF-8" action={@props.submitURL} onSubmit=(@validate) method="POST">
        <input type="hidden" name="program"
         ref="program-id" value={@props.currentProgram.program_id}/>

        <input type="hidden" name="update"
         ref="program-id" value="1"/>

         <div className="input-group-container" id="program-input">
          {fields}
        </div>
        <div className="button_container"id="save_button_container">
          <button className= "submit-button" type="submit" id="program-submit">{buttonText}</button>
        </div>
      </form>
    </div>









