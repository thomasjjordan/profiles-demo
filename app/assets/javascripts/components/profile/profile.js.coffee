@ProfileInput = React.createClass
  getInitialState: ->
    inputValue: @props.inputValue,
    label: @props.label
    errorClass: @props.errorClass

  handleChange: (e) ->
    @setState inputValue: e.target.value
    @setState label: e.target.value
    @setState errorClass: e.target.value

  createLabel: ->
    { __html: @props.label }


  render: ->
      <div className="input-group-container">
        <div dangerouslySetInnerHTML={@createLabel()} />
        <input type="text" className="input-group-input w3-text-theme-border #{@props.errorClass}"
               name="profile[#{@props.inputType}]" value={@state.inputValue}
               onChange={@handleChange}/>
      </div>

@ProfilePasswordInput = React.createClass
  getInitialState: ->
    inputValue: @props.inputValue

  handleChange: (e) ->
    @setState inputValue: e.target.value

  createLabel: ->
    { __html: @props.label }

  render: ->
      <div className="input-group-container">

        <div dangerouslySetInnerHTML={@createLabel()} />
        <input type="password" className="input-group-input w3-text-theme-border #{@props.errorClass}"
               name="profile[#{@props.inputType}]" value={@state.inputValue}
               onChange={@handleChange}/>
      </div>


@ProfileSection = React.createClass
  render: ->
    <div className="w3-col m12 l6 w3-container">
      <div className="description-block">
        <div className="description-container">
          <h2 className="w3-text-theme-primary">{@props.section_header}</h2>
          <p className="description-text w3-text-theme">{@props.section_description}</p>
        </div>
      </div>
    </div>

@ProfileSubmit = React.createClass
  # submitProfile: ->
  #   formData =  jQuery('form.profiles_form').serialize()
  #   console.log formData
  #   jQuery.post @props.profile_submit_url, jQuery('form.profiles_form').serialize(),
  #     (data) =>
  #       console.log data.status

  # <button type="button" id="profile_submit" onClick={@submitProfile}>{@props.button_text}</button>

  render: ->
    <button className="submit-button" type="submit" id="profile_submit">{@props.button_text}</button>



