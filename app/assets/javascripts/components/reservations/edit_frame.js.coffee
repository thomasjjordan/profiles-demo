@EditFrame = React.createClass
  displayName: "EditFrame"

  getInitialState: ->
    sizingInterval: undefined,
    loadingInterval: undefined,
    loadingURL: true,
    loadingFrame: true,
    resEditURL: undefined

  componentDidMount: ->
    @getResEditURL()
    App.scrollToTheTop()
    @setState sizingInterval: window.setInterval(@autoResizeFrame, 400)

  componentWillUnmount: -> clearInterval @state.sizingInterval

  autoResizeFrame: ->
    iframe = React.findDOMNode(@refs.iframe)
    return unless iframe
    frameBody = iframe.contentDocument.getElementsByTagName('body')[0]

    if frameBody
      contentSize = frameBody.offsetHeight + 'px'
      frameSize = iframe.style.height
      iframe.style.height = contentSize if frameSize != contentSize
      App.autoResizeParent()

  getResEditURL: ->
    jQuery.getJSON(@props.getResEditURL, id: @props.record.ID)
      .then (data) => @setState loadingURL: false, resEditURL: data.url

  iframeLoaded: ->
    # Can't necessarily trust onLoad, let's get secondary confirmation.
    checkFrameLoad = =>
      # If we can access the iframe contents then it must have loaded.
      if jQuery('iframe').contents().find('body div').length
        clearInterval @state.loadingInterval
        App.scrollToTheTop()
        @setState loadingFrame: false
    @setState loadingInterval: window.setInterval(checkFrameLoad, 400)

  render: ->
    if @state.loadingURL || @state.loadingFrame
      loader = <div className="edit-frame-loader">
                 <div>
                   <i className="fa fa-spinner fa-spin"></i>Loading...
                 </div>
               </div>

    <div>
      {loader}
      <a href="#"
         style={{marginLeft: '11px'}}
         onClick={@props.back}>{@props.backCaption}</a>
      <iframe ref="iframe"
              allowTransparency="true"
              marginWidth="0"
              marginHeight="0"
              vspace="0" hspace="0"
              width="100%" height="600"
              style={{overflow: 'hidden', minHeight: '600px'}}
              scrolling="no"
              src={@state.resEditURL}
              frameBorder="0"
              onLoad={@iframeLoaded}
              ></iframe>
    </div>
