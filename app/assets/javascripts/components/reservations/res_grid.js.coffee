@ResGrid = React.createClass
  displayName: "ResGrid"

  getInitialState: ->
    currentRecord: null
    requestError: false

  setMsg: (msg, error) ->
    className = if error then "error_box" else "notice_box"
    jQuery('.info-box-container')
      .append("<p class=" +className+ " >" + msg + "</p>")
    setTimeout((-> jQuery('.info-box-container').html("<p></p>")), 4000);

  render: ->
    gridTable = <GridTable grid={@props.grid}
                  detailsCaption={@props.detailsCaption}
                  showDetailsButton={@props.showDetailsButton}
                  recordSelected={@props.recordSelected}
                  windowWidth={@props.windowWidth}/>

    # modal = <Modal isOpen={@state.isModalOpen}
    #           closeModal={@closeModal}
    #           confirmCancleMsg={@props.confirmCancleMsg}
    #           cancellationResult={@state.cancellationResult}
    #           cancelRequestSent={@state.cancelRequestSent}
    #           cancelRes={@cancelRes}
    #           currentRecord={@state.currentRecord}
    #           requestError={@state.requestError}
    #           transitionName="modal-anim"
    #           />

    className = if (@props.windowWidth <= 768)
                  "reservations-grid w3-responsive w3-small"
                else
                  "reservations-grid w3-responsive"

    <div className= "grid-section">
      <div className={className} id='grid'>
        {gridTable}
      </div>
    </div>


