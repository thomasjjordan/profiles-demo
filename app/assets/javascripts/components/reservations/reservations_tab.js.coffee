@ReservationsTab = React.createClass
  displayName: "ReservationsTab"

  getInitialState: ->
    editingRecord: false
    gdfFile: ""
    gridIsLoading: false
    gridIsUpdating: false
    noGridSelected: true
    noRecords: true
    windowWidth: window.innerWidth

  handleResize: (e) ->
    @setState windowWidth: window.innerWidth

  componentDidMount: ->
    window.addEventListener('resize', @handleResize)

  componentWillUnmount: ->
    window.removeEventListener('resize', @handleResize)

  newGrid: (grid) ->
    @setState grid: grid

  updateGrid: (status) ->
    @setState gridIsUpdating: status


  handleGridChange: (grid) ->
    if !grid
      @setState noGridSelected: true
    else
      @setState gdfFile: grid, gridIsLoading: true, noGridSelected: false
      jQuery.get @props.grid_info_path,
        {'grid_info': grid},
        (data) =>
          @setState {
                      grid: data.selected_grid,
                      gridIsLoading: false,
                      noRecords: (data.selected_grid.records.length == 0) }
          App.autoResizeParent()

  recordSelected: (record) -> @setState editingRecord: record

  renderEditFrame: ->
    stopEditing = => @setState editingRecord: false
    <EditFrame record={@state.editingRecord}
               back={stopEditing}
               backCaption={@props.edit_back_caption}
               getResEditURL={@props.get_res_edit_url}/>

  renderGrids: ->
    if @state.gridIsUpdating
      gridContent =
        <div id="updateing">
        <p id="update-msg">{@props.grid_update_msg}</p>
          <i className="fa fa-refresh fa-spin fa-2x w3-theme-inverse"
            id='grid-updater'></i>
        </div>

    else if @state.noGridSelected && !@state.gridIsLoading
      gridContent = <p>{@props.no_grid_selected_msg}</p>

    else if @state.noRecords && !@state.gridIsLoading
      gridContent = <p>{@props.no_grid_records_msg}</p>


    else unless @state.gridIsLoading && !@state.gridIsUpdating
      gridContent = <ResGrid
                      gdfFile={@state.gdfFile}
                      newGrid={@newGrid}
                      grid={@state.grid}
                      detailsCaption={@props.details_caption}
                      showDetailsButton={@props.show_details_button}
                      windowWidth={@state.windowWidth}
                      noRecordsMsg={@props.no_records_msg}
                      recordSelected={@recordSelected}
                      updateGrid={@updateGrid}
                      />

    if @state.gridIsLoading
      loader = <div id="loader">
          <i className="fa fa-spinner fa-pulse fa-3x w3-theme-inverse" id='grid-spinner'></i>
        </div>

    gridSelector =  <GridSelector
                      key="gridSelector"
                      selectValue={@selectValue}
                      gridSelectLabel={@props.grid_select_label}
                      gridSelectText={@props.grid_select_text}
                      handleGridChange={@handleGridChange}
                      availableFiles={@props.available_files}
                      />

    divStyle = {marginBottom: 25};

    <div>
      <div className="w3-col l12 w3-container">
        {gridSelector}
      </div>
      <div className="w3-col l12 w3-container">
        {loader}
        <div style={divStyle}>
          {gridContent}
        </div>
      </div>
    </div>

  render: ->
    content = if @state.editingRecord then @renderEditFrame() else @renderGrids()
    <div className="ReservationsTab">{content}</div>
