@GridTable.Column = React.createClass
  displayName: "GridTable Column"
  render: ->
    <th className="grid-table-header w3-theme">{@props.column.name}</th>