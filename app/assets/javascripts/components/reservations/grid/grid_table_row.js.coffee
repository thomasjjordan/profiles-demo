@GridTable.Row = React.createClass
  displayName: "GridTableRow"

  render: ->

    editClass = if (@props.windowWidth <= 768)
                  "edit-cell w3-theme"
                else
                  "edit-cell"


    # cancelButton = <CancelButton
    #                 cancelResBtn={@props.cancelResBtn}
    #                 confirmCancel={@props.confirmCancel}
    #                 onClick={@props.onClick}
    #                 record={@props.record}
    #                 />

    tableData =
      for column in @props.grid.columns when !column.isHidden
        value = @props.record[column.fieldName]
        key = "#{@props.record.ID}-#{column.fieldName}"
        <GridTable.Record key={key} columnName={column.name} record={value}/>

    if @props.showDetailsButton
      editCell = <td className={editClass}>
                   <i className="fa fa-edit"></i>
                   <span>{@props.detailsCaption}</span>
                 </td>

    <tr className="grid-table-row" onClick={@props.onClick}>
      {editCell}
      {tableData}
    </tr>
