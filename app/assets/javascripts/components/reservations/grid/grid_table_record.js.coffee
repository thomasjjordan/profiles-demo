@GridTable.Record = React.createClass
  displayName: "GridTable Record"

  render: ->
    if not @props.record
      className = "no-record-data"
      recordValue = null
      noRecordValue = "--"
    else
      className = "record-data"
      recordValue = @props.record
    recordData =  <div className = "record-data-container">
                    <div className="column-name">{@props.columnName}</div>
                    <div className="record-data-value">{noRecordValue}{recordValue}</div>
                  </div>

    <td className={className}>{recordData}</td>