@GridTable = React.createClass
  displayName: "GridTable"

  selectedRecord: (record) -> @props.recordSelected(record)

  render: ->
    tableRows = @props.grid.records.map (record) =>
      if @props.showDetailsButton
        onClick = @selectedRecord.bind(this, record)
      <GridTable.Row
        key={record.ID}
        detailsCaption={@props.detailsCaption}
        showDetailsButton={@props.showDetailsButton}
        record={record}
        onClick={onClick}
        windowWidth={@props.windowWidth}
        grid={@props.grid}
        />


    tableColumns = @props.grid.columns.map (column) =>
                    <GridTable.Column
                      key={column.fieldName}
                      column={column}
                      /> unless column.isHidden

    if @props.showDetailsButton
      editCell = <th className="grid-table-header edit-cell w3-theme">&nbsp;</th>

    <div className="w3-responsive">
      <table className="w3-table w3-hoverable">
        <tbody>
          <tr>
            {editCell}
            {tableColumns}
          </tr>
          {tableRows}
        </tbody>
      </table>
    </div>
