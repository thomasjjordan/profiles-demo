@GridSelector = React.createClass
  displayName: "GridSelector"

  getInitialState: ->
    selectedGrid: null

  onGridChange: (event) ->
    grid = event.target.value
    @setState selectedGrid: grid
    @props.handleGridChange(grid)

  render: ->
    selectOptions = for file_key, file_value of @props.availableFiles
      <option key={file_key} value={file_value}>{file_key}</option>
    <div className="grid-select-block">
      <label className="input-group-label w3-text-theme" htmlFor="grid-selector">{@props.gridSelectLabel}</label>
      <div className="input-group-select w3-text-theme-border" id="grid-select">
        <div className="select-wrapper">
          <select value={@state.selectedGrid} onChange={@onGridChange} id="grid-selector">
            <option value="" >{@props.gridSelectText}</option>
            {selectOptions}
          </select>
        </div>
      </div>
    </div>