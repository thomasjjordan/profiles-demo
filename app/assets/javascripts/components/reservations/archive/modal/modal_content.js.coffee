@Modal.Content = React.createClass
  displayName: "ModalContent"
  render: ->
    <div className="w3-container">
      <div className="w3-row">
        <div className="w3-col l6 w3-center">
          <p>Press no to return to your reservations.</p>
          <span><button className="w3-btn w3-large w3-theme confirm-btn" onClick={@props.closeModal}>No</button></span>
        </div>
        <div className="w3-col l6 w3-center">
          <p>Press yes to confirm you want to cancel your reservation.</p>
           <span><button className="w3-btn w3-large w3-theme confirm-btn" onClick={@props.cancelRes}>Yes</button></span>
        </div>
      </div>
    </div>