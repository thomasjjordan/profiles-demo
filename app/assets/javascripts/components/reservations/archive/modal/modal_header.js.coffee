@Modal.Header = React.createClass
  displayName: "ModalHeader"
  render: ->
    if @props.cancelRequestSent && !@props.requestError
      headerText = "Cancellation Confirmed"
    else if @props.cancelRequestSent? && @props.requestError
      headerText = "An Error Occurred"
    else
      headerText = @props.confirmCancleMsg

    <header className="w3-container w3-theme">
      <span onClick={@props.closeModal} className="w3-closebtn">&times;</span>
      <h3>{headerText}</h3>
    </header>