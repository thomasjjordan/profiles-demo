@Modal.Message = React.createClass
  displayName: "ModalMessage"

  render: ->

    if @props.requestError
      className="modal-message error-class"
      messageIcon="fa fa-exclamation-triangle fa-lg"
      messageStyle= {color: 'red'};
    else
      className="modal-message success-class"
      messageIcon="fa fa-check-square fa-lg"
      messageStyle= {color: 'green'};


    <div className="w3-container">
      <div className="w3-row">
        <div className="w3-col l12 w3-center">
          <p className={className} style={messageStyle}>
            <i className={messageIcon} id="message-icon"></i>
            {@props.cancellationResult}
          </p>
        </div>
      </div>
    </div>