ReactCSSTransitionGroup = React.addons.CSSTransitionGroup
@Modal = React.createClass
  displayName: "Modal"

  render: ->
    if @props.cancelRequestSent
      modalBody = <Modal.Message
                    cancellationResult={@props.cancellationResult}
                    requestError={@props.requestError}
                    />
    else
      modalBody = <Modal.Content
                    closeModal={@props.closeModal}
                    cancelRes={@props.cancelRes}
                    />

    modalHeader = <Modal.Header
                    confirmCancleMsg={@props.confirmCancleMsg}
                    closeModal={@props.closeModal}
                    requestError={@props.requestError}
                    cancelRequestSent={@props.cancelRequestSent}
                    />

    if @props.isOpen
          <ReactCSSTransitionGroup transitionName={@props.transitionName}>
            <div className="CancelModal">
              <div id="cancel-modal" className="w3-modal">
                <div className="w3-modal-content" id="modal-content">
                  {modalHeader}
                  {modalBody}
                </div>
              </div>
            </div>
          </ReactCSSTransitionGroup>

    else
      <ReactCSSTransitionGroup transitionName={@props.transitionName} />
