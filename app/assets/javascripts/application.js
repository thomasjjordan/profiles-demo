// Shims needed for old browser support for React
//= require console-polyfill
//= require matchMedia
//= require es5-shim.min
//= require es5-sham.min

//= require jquery
//= require dataTables/jquery.dataTables
//= require jquery_ujs
//= require_self
//= require 'app'
// require turbolinks
//= require react
//= require react_ujs
//= require components


jQuery.noConflict();


// Ensure console doesn't cause a problem in production.
if(typeof console === "undefined") { console = { log: function() { } }; }

// YAHOO.namespace("hudson.profiles");

function addOptionToSelect(list, text, value) {
  var option = document.createElement('option');
  option.value = value;
  var labelNode = document.createTextNode(text);
  option.appendChild(labelNode);
  list.appendChild(option);
}