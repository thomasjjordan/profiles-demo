function initialize() {
  if ($('pud_internal_container'))
    initializePUD(YAHOO.hudson.profiles.display_address_lookup_tab);
	YAHOO.util.Event.addListener($('pud'), 'change', function() { loadPud(); });
	YAHOO.util.Event.addListener($('add_pud'), 'click', function() { loadPud(true); });
	YAHOO.util.Event.addListener($('pud_form'), 'submit', function(e) {
			disableSave();
			YAHOO.hudson.profiles.saveButton.set('label', 'Saving...');
	});
}

function initializePUD() {
  var showAddrLookup = jQuery('#pud_internal_container').data('lookup-enabled');

  YAHOO.hudson.profiles.saveButton = new YAHOO.widget.Button('save');
  initializeTabs(showAddrLookup);
  YAHOO.util.Event.addListener($('airport'), 'change', function() { updateLocationsContainer(); });
}

function addressResultSelected() {
  setPostKey( $('address_results_select').getValue());
  displayAddressResultDetail($('address_results_select').getValue());
}

/* Blank out the other location lists. */
function clearLocationLists(keep_type) {
    $$('.location_list').each(function(elem) {
        var elem_type = elem.id.match(/_(\w+)$/)[1];
        if (elem_type != keep_type)
            elem.setValue('');
    });

    if (!keep_type || keep_type.blank())
        setPickupKey('');
}

function disableField(field) {
  field_id = 'pud_' + field;
  hidden_field_id = 'pud_' + field + '_hidden';
  if ($(field_id)) {
    $(field_id).disable();
    $(field_id).addClassName('disabled');
    $(hidden_field_id).value = $(field_id).value;
    $(hidden_field_id).name = "pud[" +field + "]";
    $(hidden_field_id).show();
  }
}

function disableSave() {
  YAHOO.hudson.profiles.saveButton.set('disabled', true);
}

function displayAddressFields(error_msg) {
    var error_elem = $('address_error');

    $('address_results').hide();
    $('address_fields').show();
    if (!error_msg.blank()) {
        error_elem.innerHTML = error_msg;
        error_elem.show();
    } else
        error_elem.hide();
}

function displayAddressResultDetail(value) {
  if (value.blank())
    hideFieldsContainer();
  else {
    jQuery.get(App.url('address-from-lookup'),
        { search_result: value, airport: jQuery('#airport').val() },
        function (data) {
          if (data.error)
            alert(data.error);
          else {
            showFieldsContainer();
            jQuery('#pud_Address').val(data.street);
            jQuery('#pud_Zipcode').val(data.zip_code);
            jQuery('#pud_City').val(data.city);
            jQuery('#pud_State').val(data.state);
            jQuery('#pickup_key').val(data.fare_key);
            disableField('Zipcode');
            disableField('Address');
            disableField('City');
            disableField('State');
          }
        } );
  }
}

function displayAddressInfo(fareCode) {
  if (fareCode.blank()) {
    hideFieldsContainer();
  } else {
    jQuery.get(App.url('location-info'),
        { fare_code: fareCode, airport_code: jQuery('#airport').val() },
        function (data) {
          if (data.error)
            alert(data.error);
          else
            updateAddressFields(data);
        } );
  }
}

function enableDisableAddressFields(disabledLocs) {
  jQuery.each(disabledLocs, function(field, disabled) {
    if (field === 'Zip') field = 'Zipcode';

    if (disabled)
      disableField(field);
    else
      enableField(field);
  });
}

function enableField(field) {
  field_id = 'pud_' + field;
  hidden_field_id = 'pud_' + field + '_hidden';
  if ($(field_id)) {
    $(field_id).enable();
    $(field_id).removeClassName('disabled');
    $(hidden_field_id).value = '';
    $(hidden_field_id).name = 'disabled[' + field + ']';
    $(hidden_field_id).hide();
  }
}

function enableSave() {
  YAHOO.hudson.profiles.saveButton.set('disabled', false);
}

function hideAddressLoadingIndicator() {
    $('address_loading').hide();
}

function hideAddressPresetTab() {
    var tabs = new YAHOO.widget.TabView('location_search_tabs');
    if (tabs.getTab(2))
        tabs.removeTab(tabs.getTab(2));
}

function hideFieldsContainer() {
  Effect.Fade('fields_container', {duration: 0.25});
  disableSave();
}

function initializeTabs(show_addr_lookup) {
    var tabs = new YAHOO.widget.TabView('location_search_tabs'),
        address, city, state, zip;

    /* Hide the address lookup tab if configured. */
    if (!show_addr_lookup) {
        $('address_lookup_tab_label').hide();
        $('address_lookup_tab_contents').hide();
    }

    $('location_search_tabs').show();

    /* "Try Again" link */
    YAHOO.util.Event.addListener($('try_again'), 'click', function(event){
        displayAddressFields('');
        setPostKey('');
    });

    /* Loc Type */
    YAHOO.util.Event.addListener($('pud_Type'), 'change', function() { setBusinessNameVisibility(); });

    /* Location lists */
    $$('.location_list').each(function(elem) {
        YAHOO.util.Event.addListener(elem, 'change', function(event){
            var type = YAHOO.util.Event.getTarget(event).id.match(/_(\w+)$/)[1];
            clearLocationLists(type);

            /* Blank out the address lookup section. */
            displayAddressFields('');
            setPostKey('');
            setPickupKey(elem.getValue());
            displayAddressInfo(elem.getValue());
            hideAddressPresetTab();
        });
    });


    /* If displaying address lookup tab */
    if ($('address_fields')) {
        /* If post_key is already set, display it.  */
        if (!$F('post_key').blank()) {
            /* Select address tab. */
            tabs.selectTab(1);

            /* Make sure address results are displayed */
            $("address_results").show();
            $('address_fields').hide();
        }

        /* If address has been preset, select that tab. */
        if ($('address_preset') && !$F('address_preset').blank())
            tabs.selectTab(2);

        /* Location Search buttons */
        var button = new YAHOO.widget.Button('loc_search');
        button.on('click', function(event) {
            /* Hide fields, display loading */
            $('address_fields').fade({
                duration: 0.4,
                afterFinish: function(){
                    $('address_loading').show();

                    if ($('address'))
                        address = $F('address');
                    if ($('city'))
                        city = $F('city');
                    if ($('state'))
                        state = $F('state');
                    if ($('zip'))
                        zip = $F('zip');

                    jQuery.get(App.url('address-lookup'),
                        { address: address,
                          city: city,
                          state: state,
                          zip: zip,
                          airport: jQuery('#airport').val() },
                        function (data) {
                          hideAddressLoadingIndicator();

                          if (data.error)
                            displayAddressFields(data.error);
                          else {
                            jQuery("#address_results_container").html(data.content);
                            jQuery('#address_results').show();
                            clearLocationLists();
                            hideAddressPresetTab();
                            hideFieldsContainer();
                          }
                        });
                }
            });
        });
    }
}

function loadPud(isNew) {
  var id;

  $('pud_container').hide();
  $('loading_indicator').show();

  if (!isNew)
    id = jQuery('#pud').val();

  jQuery("#pud_container").load(App.url('pud'), { id: id },
      function () {
        initializePUD();
        jQuery('#pud_container').show();
        if (isNew)
          hideFieldsContainer();
        else
          showFieldsContainer();
        jQuery('#loading_indicator').hide();
      });
}

function setBusinessNameVisibility() {
  if ($('pud_Type').value == 'H')
    $('business_name_container').hide();
  else
    $('business_name_container').show();
}

function setPickupKey(pickup_key) {
    $('pickup_key').value = pickup_key;
}

function setPostKey(post_key) {
    $('post_key').value = post_key;
}

function showFieldsContainer() {
  Effect.Appear('fields_container', {duration: 0.25});
  enableSave();
}

function updateAddressFields(loc) {
  console.dir(loc);
  showFieldsContainer();

  $('pud_Address').value = loc.Address;
  $('pud_Zipcode').value = loc.Zip;
  $('pud_Telephone').value = loc.TelephoneNum;
  $('pud_City').value = loc.City;
  $('pud_State').value = loc.State;

  enableDisableAddressFields(loc.readonly);
}

function updateLocationsContainer() {
  Effect.Fade('pud_container', {duration: 0.25});
  $('loading_indicator').show();

  jQuery("#pud_container").load(App.url('pud'),
      { airport: jQuery('#airport').val() },
      function () {
        initializePUD();
        Effect.Appear('pud_container', {duration: 0.25});
        hideFieldsContainer();
        $('loading_indicator').hide();
      });
}