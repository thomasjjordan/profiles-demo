jQuery(window).load ->
  jQuery('#contacts').dataTable
    scrollY: "200px"
    scrollCollapse: true
    paging: false
    bJQueryUI: true

  App.autoResizeParent()

YAHOO.util.Event.onContentReady "contact_yui_button", ->
  new YAHOO.widget.Button("contact_yui_button")

YAHOO.util.Event.onContentReady "add_contact_yui_button", ->
  new YAHOO.widget.Button("add_contact_yui_button")

YAHOO.util.Event.onContentReady "new_contact_yui_button", ->
  new YAHOO.widget.Button("new_contact_yui_button")




