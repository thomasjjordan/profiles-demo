// For each link with class of "cancel_link", create an onclick event that sends a confirm with the
// confirm_text text in it, and if confirmed, put up "cancelling" animation window.
YAHOO.util.Event.onDOMReady(function() {
  YAHOO.util.Dom.getElementsByClassName('cancel_link').each(function(elem) {
//  alert("Found: " + elem);
    YAHOO.util.Event.addListener(elem, "click", function(e) {
      if (confirm(this.getAttribute('confirm_text'))) {
        YAHOO.hudson.profiles.cancelContainer.show();
      } else
        YAHOO.util.Event.preventDefault(e);
    });
  });

  // "Cancelling..." window
  YAHOO.hudson.profiles.cancelContainer = new YAHOO.widget.Panel('cancel',
    { width: '240px',
      fixedcenter: true,
      close: false,
      draggable: false,
      zindex: 4,
      modal: true,
      visible: false
    }
  );
  YAHOO.hudson.profiles.cancelContainer.setHeader("Cancelling, please wait...");
//  YAHOO.hudson.profiles.cancelContainer.setBody('<img src="hudson_reservation_detail/images/rel_interstitial_loading.gif" />');
  YAHOO.hudson.profiles.cancelContainer.render(document.body);


  // "Loading..." window
  YAHOO.hudson.profiles.loadingContainer = new YAHOO.widget.Panel('loading',
    { width: '240px',
      fixedcenter: true,
      close: false,
      draggable: false,
      zindex: 4,
      modal: false,
      visible: false
    }
  );
  YAHOO.hudson.profiles.loadingContainer.setHeader("Loading, please wait...");
//  YAHOO.hudson.profiles.loadingContainer.setBody('<img src="hudson_reservation_detail/images/rel_interstitial_loading.gif" />');
  YAHOO.hudson.profiles.loadingContainer.render(document.body);
});