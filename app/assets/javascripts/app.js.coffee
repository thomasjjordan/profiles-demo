class App
  constructor: ->
    jQuery('document').ready =>
      if (typeof(initialize) == 'function')
        initialize();

      jQuery.ajaxSetup(
        data: { site_id: @config('site') },
        global: true,
        beforeSend: (xhr) ->
          xhr.setRequestHeader('X-CSRF-Token', jQuery('meta[name="csrf-token"]').attr('content')) )
      jQuery(document).ajaxComplete (event,xhr) ->
        data = jQuery.parseJSON(xhr.responseText) if xhr.responseText?.charAt(0) is '{'
        window.location.href = data.redirect_to if (data?.redirect_to?)
      @scrollToTheTop()
      @autoResizeParent()
    jQuery(window).on('page:restore', @autoResizeParent)
    jQuery(document).on('page:load', @autoResizeParent)
    jQuery(window).on('load', @autoResizeParent)

  autoResizeParent: =>
    return unless @myFrame()
    @myFrame().style.height = "#{@myHeight()}px"
    parent.App?.autoResizeParent?()

  config: (key) -> jQuery('body').data(key)

  devMode: -> @config("env") is 'development'

  ensureSSL: ->
    if !@devMode() && window.location.protocol != "https:"
      window.location.href = "https:" + window.location.href.substring(window.location.protocol.length)

  computedStyle: (prop) ->
    el = document.body
    retVal = document.defaultView.getComputedStyle(el, null)
    retVal = if retVal? then retVal[prop] else 0
    parseInt(retVal, 10)

  myFrame: -> window.frameElement

  myHeight: ->
    height = document.body.offsetHeight +
      @computedStyle('marginTop') +
      @computedStyle('marginBottom')

    if height < 250 then 250 else height
    # jQuery('html').height() + 60

  parent: ->
    try
      return parent if parent.document
    catch
      null

  parentDocument: -> try parent.document catch e

  parentFrameId: ->
    try
      return window.frameElement?.id
    catch
      null

  parentScrollToTheTop: ->
    if @parentDocument()?
      iframeElem = @parentDocument()?.getElementById(@parentFrameId())
      if iframeElem
        rect = iframeElem.getBoundingClientRect()
        @parent()?.scrollBy(0, rect.top - 50)

  scrollToTheBottom: ->
    jQuery("html, body").animate scrollTop: jQuery(document).height(), 1000

  scrollToTheTop: ->
    jQuery("html, body").animate scrollTop: 0, "slow"
    @parentScrollToTheTop()

  url: (urlName) ->
    url = @config('urls')[urlName]
    url || throw "No URL found for #{urlName}"

root = exports ? this
root.App = new App()
