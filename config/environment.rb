# RELEASE_VERSION is used to determine where to deploy the app to.
# For example, if RELEASE_VERSION is '5' then the app might be deployed
# to /a/vmdt5 on the servers.
RELEASE_VERSION = '6'

# Load the rails application
require File.expand_path('../application', __FILE__)

# Initialize the rails application
ProfilesDialog::Application.initialize!
