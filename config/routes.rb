ProfilesDialog::Application.routes.draw do
  scope ENV['RAILS_RELATIVE_URL_ROOT'] || '/' do
    mount HudsonBase::Engine, :at  => 'hudson_base', :as => 'hudson_base'

    match 'forgot_my_password' => 'user#forgot_my_password', as: :forgot_password, via: :all
    match 'locations' => 'locations#index', as: :locations, via: :all
    match 'login' => 'user#login', as: :login, via: :all
    match 'logout' => 'user#logout', as: :logout, via: :all
    match 'new' => 'profile#new', as: :new, via: :all
    match 'points' => 'points#index', as: :points, via: :all
    # match 'points_clone' => 'points#index_clone', as: :points_clone, via: :all
    match 'contacts' => 'contacts#index', as: :contacts, via: :get


    match 'reservations' => 'reservations#index', as: :reservations, via: :all
    get 'reservation' => 'reservations#reservation', as: :reservation
    match 'reservation/grid_info' => 'reservations#grid_info', as: :grid_info, via: :all
    get 'reservation/res_edit_address' => 'reservations#res_edit_address',
         as: :get_res_edit_address

    match 'profile' => 'profile#profile', as: :profile, via: :all
    get 'address_lookup' => 'locations#address_lookup'
    get 'location_info' => 'locations#location_info'
    get 'address_from_lookup' => 'locations#address_from_lookup'
    get "contacts/new" => 'contacts#new'
    get 'contacts/:id/edit' => 'contacts#edit', as: :contact
    post 'contacts/:id/delete' => 'contacts#delete', as: :delete_contact
    post 'contacts/:id' => 'contacts#save' , as: :update_contact
    post 'contacts' => 'contacts#create' , as: :create_contact

    post 'pud' => 'locations#pud'
    match ':action' => 'profile#index', via: :all
    match ':controller/:action' => '#index', via: :all

  end
end
