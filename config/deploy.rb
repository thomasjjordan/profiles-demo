# Deploy script as adapted from the book "Deploying Rails"

# Deploying to production:
#> cap deploy

# Deploying to demo:
#> cap demo deploy

# Setting up a new app version:
#> cap new_version:setup
#-------------

# ----- Configuration you may need to change:

# If you change the name or location of this app:
set :base_app, 'profiles' # The base pathname for this app on the server.
set :app_version, '6'

# If you change which ruby this app uses:
#   change the PassengerRuby line in the :update_htaccess task below.
#   and change this line:
set :rvm_ruby_string, '2.1.1'


# -----
# ----- You shouldn't have to change anything below this line!
# -----

require 'net/http'
require 'bundler/capistrano'
require 'rvm/capistrano'
require 'capistrano_colors'

set :application, "#{base_app}#{app_version}"
set :scm, :git

# Release environment config (demo, production)
role(:web) { Net::HTTP.get('web10.hudsonltd.net', '/rails/host_list').split("\n") }
set :stages, %w(demo production)
set :default_stage, 'production'
require 'capistrano/ext/multistage'

# Deployment config
set :user, 'perladm'
set :group, 'perladm'
set :rvm_type, :system
set :deploy_to, "/var/www/rails_apps/#{application}/prod"
set :use_sudo, false
set :deploy_via, :remote_cache
set :copy_strategy, :export
set :keep_releases, 3

# Figure out deploy branch.
current_branch = `git branch`.match(/\* (\S+)\s/m)[1]
set :branch, ENV['branch'] || current_branch || "master"
if current_branch != 'master'
  puts <<-WARN

  ========================================================================

    WARNING: You're deploying from the
       << #{current_branch} >> branch
    instead of master.

    That's fine if you intended to, just making sure.

  ========================================================================

  WARN
  answer = Capistrano::CLI.ui.ask "  Are you sure you want to continue? (Y to continue) "
  exit if answer.upcase != 'Y'
end

namespace :deploy do
  task :start do ; end
  task :stop do ; end

  desc "Restart the application"
  task :restart, :roles => :web, :except => { :no_release => true } do
    run "#{try_sudo} touch #{File.join(current_path, 'tmp', 'restart.txt')}"
  end
  after "deploy:restart", "deploy:cleanup"

  desc "Clear the remote cache"
  task :clear_remote_cache, :roles=>[:web] do
    run "rm -rf #{shared_path}/cached-copy"
    run "rm -rf /var/www/rails_apps/#{application}/dvl/shared/cached-copy"
  end

  desc "Copy the database.yml file into the latest release"
  task :copy_in_database_yml do
    run "cp #{latest_release}/config/database.#{base_app}.yml #{latest_release}/config/database.yml"
    # run "#{try_sudo} touch /var/www/rails_apps/#{application}/dvl/shared/manifest.yml"
    # This will ultimately be how this task is executed.
    # run "cp #{shared_path}/config/database.yml #{latest_release}/config/"
  end

  desc "Create a .htaccess file for Phusion Passenger."
  task :update_htaccess do
    template = <<-EOF
PassengerEnabled on
RailsEnv #{rails_env}
SetEnv RAILS_RELATIVE_URL_ROOT /a/r/#{application}
PassengerRuby /usr/local/rvm/wrappers/ruby-2.1.1/ruby
PassengerAppRoot #{deploy_to}/current

PassengerMinInstances 1
    EOF
    put template, "#{release_path}/public/.htaccess"
  end
  before "deploy:restart", "deploy:update_htaccess"
end
before "deploy:assets:precompile", "deploy:copy_in_database_yml"

# Tasks for creating a new application version.
# Be sure to update the :app_version above before running.
namespace :new_version do
  desc "Setup a new application version on the servers."
  task :setup do
    create_phusion_symlinks
    system("cap deploy:setup")
    system("cap demo deploy:setup")
  end

  task :create_phusion_symlinks do
    # Ensure the base application /a/<app> exists.
    run "ln -sf /var/www/rails_apps/redirector/prod/current/public /var/www/html/a/#{base_app}"
    run "ln -sf /var/www/rails_apps/redirector/prod/current/public /var/www/html/a/#{base_app}_demo"

    # Create the /a/r/<application> links for prod and demo.
    run "ln -sf /var/www/rails_apps/#{application}/prod/current/public /var/www/html/a/r/#{application}"
    run "ln -sf /var/www/rails_apps/#{application}/dvl/current/public /var/www/html/a/r/#{application}_demo"
  end
end