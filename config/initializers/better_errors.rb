BetterErrors.editor = :sublime if defined? BetterErrors

if defined?(BetterErrors)
  # Opening files
  BetterErrors.editor = proc { |full_path, line|
    vagrant_host_path = ENV["VAGRANT_HOST_PATH"].sub("/vagrant", "")
    vagrant_host_path << Rails.root.to_s.sub("/home/vagrant/apps", "")
    full_path = full_path.sub(Rails.root.to_s, vagrant_host_path)
    "subl://open?url=file://#{full_path}&line=#{line}"
  }

  # Allowing host
  host = ENV["SSH_CLIENT"] ? ENV["SSH_CLIENT"].match(/\A([^\s]*)/)[1] : nil
  BetterErrors::Middleware.allow_ip! host if [:development, :test].member?(Rails.env.to_sym) && host
end