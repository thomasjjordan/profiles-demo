require File.expand_path('../boot', __FILE__)

require 'rails/all'

Bundler.require(:default, Rails.env)

module ProfilesDialog
  class Application < Rails::Application
    config.action_view.embed_authenticity_token_in_remote_forms = true
    config.sass.load_paths << File.expand_path('../../vendor/assets/stylesheets/')

    # Include React Addons in react build.
    config.react.addons = true
  end
end
