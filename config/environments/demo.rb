ProfilesDialog::Application.configure do
  config.cache_classes = true
  config.eager_load = true
  config.consider_all_requests_local       = false
  config.action_controller.perform_caching = true
  config.action_dispatch.x_sendfile_header = "X-Sendfile"
  config.cache_store = :dalli_store, { namespace: 'rails4', :expires_in => 21600}
  config.serve_static_assets = false
  config.i18n.fallbacks = true
  config.active_support.deprecation = :notify
  config.relative_url_root = "/a/r/profiles#{RELEASE_VERSION}_demo"

  config.log_tags = [
    :subdomain,
    :remote_ip,
    lambda { |request| "#{request.uuid}"[0..15] }
  ]

  config.middleware.use ExceptionNotification::Rack,
    email: {
      email_prefix: "[ERR] ",
      sender_address: %{ "** Profiles#{RELEASE_VERSION} Error **" <alarmlist@hudsonltd.com> },
      exception_recipients: %w{rails-dev@hudsonltd.com},
      email_format: :html,
      normalize_subject: true
    },
    ignore_if: ->(env, exception) {
      exception.message =~ /invalid %-encoding/ ||
        env.include?('HTTP_ACUNETIX_PRODUCT')
    },
    ignore_crawlers: %w{Googlebot bingbot msnbot mlbot ScanAlert EasouSpider aiHitBot Baiduspider}

  config.assets.compile = false
  config.assets.digest = true
  config.assets.precompile += %w[*.png *.jpg *.jpeg *.gif
    contacts.js locations.js color.css layout.css profile/*.css
    normalize.css typography.css reservations.js yahoo-dom-event.js form_changed.*]
  config.assets.css_compressor = :sass
  config.assets.js_compressor = :uglifier
end

::ActiveSupport::Deprecation.silenced = true
