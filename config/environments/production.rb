ProfilesDialog::Application.configure do

  # Production React Build
  config.react.variant = :production

  config.cache_classes = true
  config.eager_load = true
  config.consider_all_requests_local       = false
  config.action_controller.perform_caching = true
  config.cache_store = :dalli_store,
      { namespace: 'rails4', :expires_in => 21600}
  config.serve_static_assets = false
  config.assets.js_compressor = :uglifier
  config.assets.css_compressor = :sass
  config.assets.compile = false
  config.assets.digest = true
  config.assets.version = '1.1'
  config.log_level = :info

  config.log_tags = [
    :subdomain,
    :remote_ip,
    lambda { |request| "#{request.uuid}"[0..15] }
  ]
  config.i18n.fallbacks = true
  config.active_support.deprecation = :notify
  config.relative_url_root = "/a/r/profiles#{RELEASE_VERSION}"

  config.middleware.use ExceptionNotification::Rack,
    email: {
      email_prefix: "[ERR] ",
      sender_address: %{ "** Profiles#{RELEASE_VERSION} Error **" <alarmlist@hudsonltd.com> },
      exception_recipients: %w{alarmlist@hudsonltd.com},
      normalize_subject: true,
      email_format: :html
    },
    ignore_if: ->(env, exception) {
      exception.message =~ /invalid %-encoding/ ||
        env.include?('HTTP_ACUNETIX_PRODUCT')
    },
    ignore_crawlers: %w{Googlebot bingbot msnbot mlbot ScanAlert EasouSpider aiHitBot Baiduspider}

  config.assets.precompile += %w[*.png *.jpg *.jpeg *.gif
    contacts.js locations.js color.css layout.css profile/*.css
    normalize.css typography.css reservations.js yahoo-dom-event.js form_changed.*]
end
