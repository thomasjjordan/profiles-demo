ProfilesDialog::Application.configure do
# Development React Build
config.react.variant = :development

  config.cache_classes = false
  config.eager_load = false
  config.cache_store = :dalli_store,
      { namespace: 'rails4', :expires_in => 21600}

  config.consider_all_requests_local       = true
  config.action_controller.perform_caching = false
  config.action_mailer.raise_delivery_errors = false
  config.active_support.deprecation = :log
  config.active_record.migration_error = :page_load
  config.assets.debug = false
  config.action_controller.action_on_unpermitted_parameters = :raise
  config.assets.precompile += %w[*.png *.jpg *.jpeg *.gif
    locations.js *.css profile/*.css
    reservations.js yahoo-dom-event.js form_changed.* contacts.* ]
end